package me.randomhashtags.randomsky;

import me.randomhashtags.randomsky.api.*;
import me.randomhashtags.randomsky.api.unfinished.CollectionChests;
import me.randomhashtags.randomsky.utils.GivedpItem;
import me.randomhashtags.randomsky.utils.Metrics;
import me.randomhashtags.randomsky.utils.RSEvents;
import me.randomhashtags.randomsky.utils.Updater;
import me.randomhashtags.randomsky.utils.supported.Placeholders;
import me.randomhashtags.randomsky.utils.supported.VaultAPI;
import me.randomhashtags.randomsky.utils.universal.UVersion;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SuppressWarnings({"unchecked"})
public final class RandomSky extends JavaPlugin {

    public static RandomSky getPlugin;
    private final String v = Bukkit.getVersion();
    private FileConfiguration config;
    private PluginManager pm;

    private Adventures adventures;
    private Alliances alliances;
    private AuctionHouse ah;
    private CoinFlip coinflip;
    private CollectionChests collectionChests;
    private CustomCrafting customcrafting;
    private CustomEnchants customenchants;
    private Homes homes;
    private Islands islands;
    private IslandChallenges islandChallenges;
    private IslandFarming islandFarming;
    private IslandLevels islandLevels;
    private IslandMining islandMining;
    private IslandPermissionBlocks islandPermissionBlocks;
    private IslandSlayer islandSlayer;
    private ItemFilter itemfilter;
    private ItemRecipes itemrecipes;
    private Jackpot jackpot;
    private Kits kits;
    private Lootboxes lootboxes;
    private Pets pets;
    private PlayerRanks playerranks;
    private PlayerSkills playerskills;
    private RepairScrolls repairscrolls;
    private SecondaryEvents se;
    private Shop shop;
    private SkyKits skykits;

    private RSEvents rsevents;
    private UVersion uversion;
    private RandomSkyAPI api;
    private GivedpItem givedpitem;
    private VaultAPI vapi;

    private SimpleCommandMap commandMap;
    private HashMap<String, Command> knownCommands;
    private HashMap<String, PluginCommand> Y = new HashMap<>(), originalCommands = new HashMap<>();

    public void onEnable() {
        getPlugin = this;

        try {
            commandMap = (SimpleCommandMap) getPrivateField(getServer().getPluginManager(), "commandMap");
            knownCommands = (HashMap<String, Command>) getPrivateField(commandMap, "knownCommands");
            Y = new HashMap<>();
        } catch (Exception e) {
            e.printStackTrace();
        }

        vapi = VaultAPI.getVaultAPI();
        vapi.setupEconomy();
        vapi.setupPermissions();
        uversion = UVersion.getUVersion();
        api = RandomSkyAPI.getAPI();
        givedpitem = GivedpItem.getGivedpItem();

        final Metrics m = new Metrics(this);
        m.addCustomChart(new Metrics.SimplePie("server_version", () -> Bukkit.getVersion()));
        m.addCustomChart(new Metrics.SimplePie("user", () -> "Discord"));
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomSky] &eUser: &f%%__USER__%%"));

        adventures = Adventures.getAdventures();
        alliances = Alliances.getAlliances();
        ah = AuctionHouse.getAuctionHouse();
        coinflip = CoinFlip.getCoinFlip();
        collectionChests = CollectionChests.getCollectionChests();
        customcrafting = CustomCrafting.getCustomCrafting();
        customenchants = CustomEnchants.getCustomEnchants();
        homes = Homes.getHomes();
        islands = Islands.getIslands();
        islandChallenges = IslandChallenges.getIslandChallenges();
        islandFarming = IslandFarming.getIslandFarming();
        islandLevels = IslandLevels.getIslandLevels();
        islandMining = IslandMining.getIslandMining();
        islandPermissionBlocks = IslandPermissionBlocks.getIslandPermissionBlocks();
        islandSlayer = IslandSlayer.getIslandSlayer();
        itemfilter = ItemFilter.getItemFilter();
        itemrecipes = ItemRecipes.getItemRecipes();
        jackpot = Jackpot.getJackpot();
        kits = Kits.getKits();
        lootboxes = Lootboxes.getLootboxes();
        pets = Pets.getPets();
        playerranks = PlayerRanks.getPlayerRanks();
        playerskills = PlayerSkills.getPlayerSkills();
        repairscrolls = RepairScrolls.getRepairScrolls();
        se = SecondaryEvents.getSecondaryEvents();
        shop = Shop.getShop();
        skykits = SkyKits.getSkyKits();

        rsevents = RSEvents.getRSEvents();
        pm = Bukkit.getPluginManager();
        if(pm.isPluginEnabled("PlaceholderAPI")) {
            new Placeholders(this).hook();
        }

        enable();
    }
    public void enable() {
        checkFiles();
        api.load();
        givedpitem.load();
        se.enable();

        getCommand("randomsky").setExecutor(api);
        getCommand("toggle").setExecutor(api);
        getCommand("chat").setExecutor(api);
        try {
            addAliases("randomsky");
            addAliases("toggle");
            addAliases("chat");
        } catch(Exception e) {
            e.printStackTrace();
        }
        rsevents.enable();
        tryLoading(Feature.ISLANDS);

        tryLoading(Feature.PETS);
        tryLoading(Feature.ADVENTURES);
        tryLoading(Feature.ALLIANCES);
        tryLoading(Feature.COINFLIP);
        tryLoading(Feature.COLLECTION_CHESTS);
        tryLoading(Feature.CUSTOM_ENCHANTS);
        tryLoading(Feature.HOMES);
        tryLoading(Feature.ISLAND_CHALLENGES);
        tryLoading(Feature.ISLAND_FARMING);
        tryLoading(Feature.ISLAND_LEVELS);
        tryLoading(Feature.KITS);
        tryLoading(Feature.PLAYER_RANKS);
        tryLoading(Feature.PLAYER_SKILLS);
        tryLoading(Feature.REPAIR_SCROLLS);
        tryLoading(Feature.ISLAND_MINING);
        tryLoading(Feature.ISLAND_PERMISSION_BLOCKS);
        tryLoading(Feature.ISLAND_SLAYER);
        tryLoading(Feature.ITEM_FILTER);
        tryLoading(Feature.ITEM_RECIPES);
        tryLoading(Feature.JACKPOT);
        tryLoading(Feature.WITHDRAW);
        tryLoading(Feature.XPBOTTLE);
        tryLoading(Feature.CUSTOM_CRAFTING);
        tryLoading(Feature.LOOTBOXES);
        tryLoading(Feature.AUCTION_HOUSE);
        tryLoading(Feature.SHOP);
        tryLoading(Feature.SKY_KITS);
    }
    private void checkFiles() {
        saveDefaultConfig();
        config = getConfig();
        checkForUpdate();
        save("_Data", "other.yml");
    }

    @Override
    public void onDisable() {
        disable();
    }
    public void disable() {
        rsevents.backup(false);
        givedpitem.disable();
        adventures.disable();
        alliances.disable();
        ah.disable();
        coinflip.disable();
        collectionChests.disable();
        customcrafting.disable();
        customenchants.disable();
        homes.disable();
        islands.disable();
        islandChallenges.disable();
        islandFarming.disable();
        islandLevels.disable();
        islandMining.disable();
        islandPermissionBlocks.disable();
        islandSlayer.disable();
        itemfilter.disable();
        itemrecipes.disable();
        jackpot.disable();
        kits.disable();
        lootboxes.disable();
        pets.disable();
        playerranks.disable();
        playerskills.disable();
        repairscrolls.disable();
        se.disableBanknote();
        se.disableXpbottle();
        shop.disable();
        skykits.disable();

        rsevents.disable();
        api.disable();
    }

    public void reload() {
        disable();
        enable();
    }

    private void checkForUpdate() {
        Bukkit.getScheduler().runTaskAsynchronously(this, () -> Updater.getUpdater().checkForUpdate());
    }

    private enum Feature {
        ADVENTURES, ALLIANCES, AUCTION_HOUSE, COINFLIP, COLLECTION_CHESTS, CUSTOM_CRAFTING, CUSTOM_ENCHANTS, HOMES,
        ISLANDS,
        ISLAND_CHALLENGES, ISLAND_FARMING, ISLAND_LEVELS, ISLAND_MINING, ISLAND_PERMISSION_BLOCKS, ISLAND_SLAYER,
        ITEM_FILTER, ITEM_RECIPES, JACKPOT, KITS, LOOTBOXES, PETS,
        PLAYER_RANKS, PLAYER_SKILLS, REPAIR_SCROLLS, SHOP, SKY_KITS,
        WITHDRAW, XPBOTTLE,
    }
    public void tryLoading(Feature f) {
        try {
            boolean enabled = false;
            final HashMap<String, String> ali = new HashMap<>();
            final List<String> cmds = new ArrayList<>();
            CommandExecutor ce = null;
            if(f.equals(Feature.ADVENTURES)) {
                enabled = config.getBoolean("adventure.enabled");
                cmds.add("adventure");
                ce = adventures;
                if(enabled) {
                    adventures.enable();
                }
            } else if(f.equals(Feature.ALLIANCES)) {
                enabled = config.getBoolean("alliance.enabled");
                cmds.add("alliance");
                ce = alliances;
                if(enabled) {
                    alliances.enable();
                }
            } else if(f.equals(Feature.AUCTION_HOUSE)) {
                enabled = config.getBoolean("auction house.enabled");
                cmds.add("auctionhouse");
                ali.put("auctionhouse", "auction house");
                ce = ah;
                if(enabled) {
                    ah.enable();
                }
            } else if(f.equals(Feature.COINFLIP)) {
                enabled = config.getBoolean("coinflip.enabled");
                cmds.add("coinflip");
                ce = coinflip;
                if(enabled) {
                    coinflip.enable();
                }
            } else if(f.equals(Feature.COLLECTION_CHESTS)) {
                enabled = config.getBoolean("collection chests.enabled");
                if(enabled) {
                    collectionChests.enable();
                }
            } else if(f.equals(Feature.CUSTOM_CRAFTING)) {
                enabled = config.getBoolean("custom crafting.enabled");
                cmds.add("crafting");
                ali.put("crafting", "custom crafting");
                ce = customcrafting;
                if(enabled) {
                    customcrafting.enable();
                }
            } else if(f.equals(Feature.CUSTOM_ENCHANTS)) {
                enabled = config.getBoolean("custom enchants.enabled");
                cmds.add("enchanter");
                cmds.add("tinkerer");
                ce = customenchants;
                if(enabled) {
                    customenchants.enable();
                }
            } else if(f.equals(Feature.HOMES)) {
                enabled = config.getBoolean("homes.enabled");
                cmds.add("home");
                cmds.add("homelist");
                cmds.add("sethome");
                cmds.add("delhome");
                ce = homes;
                if(enabled) {
                    homes.enable();
                }
            } else if(f.equals(Feature.ISLANDS)) {
                enabled = true;
                cmds.add("island");
                cmds.add("origin");
                ce = islands;
                islands.enable();
            } else if(f.equals(Feature.ISLAND_CHALLENGES)) {
                enabled = config.getBoolean("island challenges.enabled");
                cmds.add("challenge");
                ali.put("challenge", "island challenges");
                ce = islandChallenges;
                if(enabled) {
                    islandChallenges.enable();
                }
            } else if(f.equals(Feature.ISLAND_FARMING)) {
                enabled = config.getBoolean("island farming.enabled");
                if(enabled) {
                    islandFarming.enable();
                }
            } else if(f.equals(Feature.ISLAND_LEVELS)) {
                enabled = config.getBoolean("island levels.enabled");
                if(enabled) {
                    islandLevels.enable();
                }
            } else if(f.equals(Feature.ISLAND_MINING)) {
                enabled = config.getBoolean("island mining.enabled");
                if(enabled) {
                    islandMining.enable();
                }
            } else if(f.equals(Feature.ISLAND_PERMISSION_BLOCKS)) {
                enabled = config.getBoolean("island permission blocks.enabled");
                if(enabled) {
                    islandPermissionBlocks.enable();
                }
            } else if(f.equals(Feature.ISLAND_SLAYER)) {
                enabled = config.getBoolean("island slayer.enabled");
                if(enabled) {
                    islandSlayer.enable();
                }
            } else if(f.equals(Feature.ITEM_FILTER)) {
                enabled = config.getBoolean("item filter.enabled");
                ce = itemfilter;
                cmds.add("filter");
                ali.put("filter", "item filter");
                if(enabled) {
                    itemfilter.enable();
                }
            } else if(f.equals(Feature.ITEM_RECIPES)) {
                enabled = config.getBoolean("item recipes.enabled");
                ce = itemrecipes;
                cmds.add("recipes");
                ali.put("recipes", "item recipes");
                if(enabled) {
                    itemrecipes.enable();
                }
            } else if(f.equals(Feature.JACKPOT)) {
                enabled = config.getBoolean("jackpot.enabled");
                ce = jackpot;
                cmds.add("jackpot");
                if(enabled) {
                    jackpot.enable();
                }
            } else if(f.equals(Feature.KITS)) {
                enabled = config.getBoolean("kit.enabled");
                cmds.add("kit");
                ce = kits;
                if(enabled) {
                    kits.enable();
                }
            } else if(f.equals(Feature.LOOTBOXES)) {
                enabled = config.getBoolean("lootbox.enabled");
                cmds.add("lootbox");
                ce = lootboxes;
                if(enabled) {
                    lootboxes.enable();
                }
            } else if(f.equals(Feature.PETS)) {
                enabled = config.getBoolean("pets.enabled");
                if(enabled) {
                    pets.enable();
                }
            } else if(f.equals(Feature.PLAYER_RANKS)) {
                enabled = config.getBoolean("player ranks.enabled");
                cmds.add("rank");
                ali.put("rank", "player ranks");
                ce = playerranks;
                if(enabled) {
                    playerranks.enable();
                }
            } else if(f.equals(Feature.PLAYER_SKILLS)) {
                enabled = config.getBoolean("player skills.enabled");
                cmds.add("skill");
                ali.put("skill", "player skills");
                ce = playerskills;
                if(enabled) {
                    playerskills.enable();
                }
            } else if(f.equals(Feature.REPAIR_SCROLLS)) {
                enabled = config.getBoolean("repair scrolls.enabled");
                if(enabled) {
                    repairscrolls.enable();
                }
            } else if(f.equals(Feature.SHOP)) {
                enabled = config.getBoolean("shop.enabled");
                cmds.add("shop");
                ce = shop;
                if(enabled) {
                    shop.enable();
                }
            } else if(f.equals(Feature.SKY_KITS)) {
                enabled = config.getBoolean("sky kits.enabled");
                cmds.add("skykit");
                ali.put("skykit", "sky kits");
                ce = skykits;
                if(enabled) {
                    skykits.enable();
                }
            } else if(f.equals(Feature.WITHDRAW)) {
                enabled = config.getBoolean("withdraw.enabled");
                cmds.add("withdraw");
                ce = se;
                if(enabled) {
                    se.enableBanknote();
                }
            } else if(f.equals(Feature.XPBOTTLE)) {
                enabled = config.getBoolean("xpbottle.enabled");
                cmds.add("xpbottle");
                ce = se;
                if(enabled) {
                    se.enableXpbottle();
                }
            }
            final boolean empty = cmds.isEmpty();
            if(enabled && !empty) {
                try {
                    int v = 0;
                    for(String cmd : cmds) {
                        PluginCommand pc = getCommand(cmd);
                        if(ce != null) {
                            if(pc == null) {
                                final String s = cmds.get(v);
                                knownCommands.put(s, originalCommands.get(s));
                                pc = (PluginCommand) knownCommands.get(s);
                            }
                            pc.setExecutor(ce);
                        }
                        final String k = pc.getName();
                        if(ali.isEmpty()) addAliases(k);
                        else addAliases(k, ali.get(k));
                        v += 1;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if(!empty) {
                for(String cmd : cmds) {
                    final PluginCommand pc = getCommand(cmd);
                    if(pc != null) unregisterPluginCommand(pc);
                }
            }
        } catch (Exception e) {
           Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomSky] &cError when loading feature &f" + f.name()));
           e.printStackTrace();
        }
    }

    private void save(String folder, String file) {
        File f;
        if(folder != null && !folder.equals(""))
            f = new File(getDataFolder() + File.separator + folder + File.separator, file);
        else
            f = new File(getDataFolder() + File.separator, file);
        if(!f.exists()) {
            f.getParentFile().mkdirs();
            saveResource(folder != null && !folder.equals("") ? folder + File.separator + file : file, false);
        }
    }
    private void addAliases(String cmd) throws Exception { addAliases(cmd, cmd); }
    private void addAliases(String cmd, String path) throws Exception {
        Object result = getPrivateField(getServer().getPluginManager(), "commandMap");
        SimpleCommandMap commandMap = (SimpleCommandMap) result;
        Object map = getPrivateField(commandMap, "knownCommands");
        @SuppressWarnings("unchecked")
        HashMap<String, Command> knownCommands = (HashMap<String, Command>) map;
        for(String alias : config.getStringList(path + ".aliases")) {
            if(!alias.equals(cmd)) {
                final String c = cmd;
                knownCommands.put(alias, getCommand(c));
                final PluginCommand pc = Bukkit.getPluginCommand(c);
                if(pc != null) pc.getAliases().add(alias);
            }
        }
    }
    /*
     * Code from "zeeveener" at https://bukkit.org/threads/how-to-unregister-commands-from-your-plugin.131808/ , edited by RandomHashTags
     */
    private Object getPrivateField(Object object, String field) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        Class<?> clazz = object.getClass();
        Field objectField = field.equals("commandMap") ? clazz.getDeclaredField(field) : field.equals("knownCommands") ? v.contains("1.8") || v.contains("1.9") || v.contains("1.10") || v.contains("1.11") || v.contains("1.12") || v.equals("1.13") ? clazz.getDeclaredField(field) : clazz.getSuperclass().getDeclaredField(field) : null;
        if(objectField == null) {
            Bukkit.broadcastMessage("objectField == null!");
            return null;
        }
        objectField.setAccessible(true);
        Object result = objectField.get(object);
        objectField.setAccessible(false);
        return result;
    }

    private void unRegisterPluginCommands(List<PluginCommand> cmds) {
        for(PluginCommand cmd : cmds) {
            if(cmd != null) unregisterPluginCommand(cmd);
        }
    }
    private void unregisterPluginCommand(PluginCommand cmd) {
        final String c = cmd.getName();
        knownCommands.remove("randomsky:" + c);
        cmd.unregister(commandMap);
        Y.remove("RandomSky:" + c);
        boolean hasOtherCmd = false;
        for(int i = 0; i < Y.keySet().size(); i++) {
            final String otherCmd = (String) Y.keySet().toArray()[i];
            if(!otherCmd.startsWith("RandomSky:") && otherCmd.split(":")[1].equals(c)) { // gives the last plugin that has the cmd.getName() the command priority
                hasOtherCmd = true;
                knownCommands.replace(c, cmd, (PluginCommand) Y.values().toArray()[i]);
            }
        }
        if(!hasOtherCmd) // removes the command completely
            knownCommands.remove(c);
    }
}
