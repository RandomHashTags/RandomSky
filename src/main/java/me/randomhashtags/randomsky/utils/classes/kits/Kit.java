package me.randomhashtags.randomsky.utils.classes.kits;

import me.randomhashtags.randomsky.RandomSkyAPI;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Kit {
    public static HashMap<String, Kit> kits;
    private static RandomSkyAPI api;

    private YamlConfiguration yml;
    private String ymlName, name;
    private long cooldown;
    private int slot;
    private ItemStack item;
    public List<String> rewards;
    public Kit(File f) {
        if(kits == null) {
            kits = new HashMap<>();
            api = RandomSkyAPI.getAPI();
        }
        yml = YamlConfiguration.loadConfiguration(f);
        ymlName = f.getName().split("\\.yml")[0];
        slot = yml.getInt("gui.slot");
        cooldown = yml.getLong("cooldown");
        kits.put(ymlName, this);
    }
    public YamlConfiguration getYaml() { return yml; }
    public String getYamlName() { return ymlName; }
    public String getName() {
        if(name == null) name = ChatColor.translateAlternateColorCodes('&', yml.getString("name"));
        return name;
    }
    public long getCooldown() { return cooldown; }
    public int getSlot() { return slot; }
    public ItemStack getItem() {
        if(item == null) item = api.d(yml, "gui");
        return item.clone();
    }

    public List<ItemStack> items() {
        final List<ItemStack> items = new ArrayList<>();
        for(String s : rewards) items.add(api.d(null, s));
        return items;
    }

    public static Kit valueOf(int slot) {
        if(kits != null) {
            for(Kit k : kits.values())
                if(k.slot == slot)
                    return k;
        }
        return null;
    }
    public static void deleteAll() {
        kits = null;
        api = null;
    }
}
