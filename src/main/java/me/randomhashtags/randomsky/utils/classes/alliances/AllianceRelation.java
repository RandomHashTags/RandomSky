package me.randomhashtags.randomsky.utils.classes.alliances;

import java.util.HashMap;

public class AllianceRelation {
    public static HashMap<String, AllianceRelation> paths;
    public final String path, color;
    public final boolean damageable;
    public AllianceRelation(String path, String color, boolean damageable) {
        if(paths == null) {
            paths = new HashMap<>();
        }
        this.path = path;
        this.color = color;
        this.damageable = damageable;
        paths.put(path, this);
    }
    public static void deleteAll() {
        paths = null;
    }
}
