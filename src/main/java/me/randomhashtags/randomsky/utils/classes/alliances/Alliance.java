package me.randomhashtags.randomsky.utils.classes.alliances;

import me.randomhashtags.randomsky.RandomSky;
import me.randomhashtags.randomsky.utils.RSPlayer;
import me.randomhashtags.randomsky.utils.classes.RSInvite;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Alliance {
    private static final String s = File.separator, folder = RandomSky.getPlugin.getDataFolder() + s + "_Data" + s + "alliances";
    public static final HashMap<String, Alliance> tags = new HashMap<>();
    public static final HashMap<UUID, Alliance> alliances = new HashMap<>(), players = new HashMap<>();

    private boolean isLoaded = false;
    private File file;
    private YamlConfiguration yml;
    private long createdTime;
    private UUID uuid, creator;
    private String tag;
    private List<AllianceMember> members = new ArrayList<>();
    private HashMap<UUID, AllianceRelationship> relations = new HashMap<>();
    private List<RSInvite> invites = new ArrayList<>();
    public Alliance(UUID creator, String tag) {
        this(System.currentTimeMillis(), UUID.randomUUID(), creator, tag, new ArrayList<>(), new ArrayList<>());
    }
    public Alliance(long createdTime, UUID uuid, UUID creator, String tag, List<AllianceMember> members, List<RSInvite> invites) {
        this.createdTime = createdTime;
        this.uuid = uuid;
        this.creator = creator;
        this.tag = tag;
        boolean is = false;
        for(AllianceMember m : members) if(m.uuid.equals(creator)) is = true;
        if(!is) members.add(new AllianceMember(createdTime, creator, AllianceRole.paths.get("leader")));
        this.members = members;
        this.invites = invites;
        created();
    }
    private void created() {
        isLoaded = true;
        tags.put(tag.toLowerCase(), this);
        alliances.put(uuid, this);
        for(AllianceMember m : members) {
            players.put(m.uuid, this);
        }
        file = new File(folder, uuid.toString() + ".yml");
        yml = YamlConfiguration.loadConfiguration(file);
        backup();
    }

    public Alliance(UUID uuid) {
        final File f = new File(folder, uuid.toString() + ".yml");
        if(!alliances.containsKey(uuid) && f.exists()) {
            file = new File(folder, uuid.toString() + ".yml");
            yml = YamlConfiguration.loadConfiguration(file);
            alliances.put(uuid, this);
            load();
            backup();
        }
    }
    public static Alliance get(UUID uuid) { return alliances.getOrDefault(uuid, new Alliance(uuid)); }

    public void backup() {
        if(creator != null) {
            final String info = createdTime + ";" + creator.toString() + ";" + tag;
            yml.set("info", info);

            String m = "";
            for(AllianceMember mem : members) {
                m = m.concat(mem.joined + ":" + mem.uuid.toString() + ":" + mem.role.path + ";");
            }
            yml.set("members", m);
            for(RSInvite in : invites) {
                yml.set("invites." + in.createdTime, in.sender.toString() + ";" + in.receiver.toString());
            }
            final HashMap<UUID, AllianceRelationship> rel = relations;
            for(UUID a : rel.keySet()) {
                final AllianceRelationship re = rel.get(a);
                final AllianceRelation r = re.relation;
                yml.set("relations." + a.toString(), re.started + ";" + r.path + ";" + re.pending);
            }

            save();
        }
    }
    public void load() {
        if(!isLoaded) {
            isLoaded = true;
            final String[] info = yml.getString("info").split(";");
            this.createdTime = Long.parseLong(info[0]);
            this.creator = UUID.fromString(info[1]);
            this.tag = info[2];
            tags.put(tag.toLowerCase(), this);

            for(String s : yml.getString("members").split(";")) {
                final String[] t = s.split(":");
                final long j = Long.parseLong(t[0]);
                final UUID m = UUID.fromString(t[1]);
                members.add(new AllianceMember(j, m, AllianceRole.paths.get(t[2])));
                players.put(m, this);
            }

            final ConfigurationSection rel = yml.getConfigurationSection("relations");
            if(rel != null) {
                for(String s : rel.getKeys(false)) {
                    final String[] p = yml.getString("relations." + s).split(":");
                    final AllianceRelationship ship = new AllianceRelationship(Long.parseLong(p[0]), AllianceRelation.paths.get(p[2]), Boolean.parseBoolean(p[3]));
                    relations.put(UUID.fromString(s), ship);
                }
            }
        }
    }
    private void save() {
        try {
            yml.save(file);
            yml = YamlConfiguration.loadConfiguration(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isLoaded() { return isLoaded; }
    public YamlConfiguration getYaml() { return yml; }
    public long getCreatedTime() { return createdTime; }
    public UUID getUUID() { return uuid; }
    public UUID getCreator() { return creator; }
    public String getTag() { return tag; }
    public List<AllianceMember> getMembers() { return members; }
    public HashMap<UUID, AllianceRelationship> getRelations() { return relations; }
    public List<RSInvite> getInvites() { return invites; }


    public List<Player> getOnlineMembers() {
        final List<Player> m = new ArrayList<>();
        for(AllianceMember mem : members) {
            final OfflinePlayer o = Bukkit.getOfflinePlayer(mem.uuid);
            if(o.isOnline()) m.add(o.getPlayer());
        }
        return m;
    }
    public void disband() {
        for(AllianceMember m : members) {
            final UUID u = m.uuid;
            players.remove(u);
            final RSPlayer rs = RSPlayer.get(u);
            rs.setAlliance(null);
        }
        for(UUID a : relations.keySet()) {
            Alliance.get(a).relations.remove(uuid);
        }
        for(RSInvite i : invites) {
            i.delete();
        }
        tags.remove(tag);
        alliances.remove(uuid);
        file.delete();
        this.members = null;
        this.relations = null;
        this.creator = null;
        this.createdTime = 0;
        this.uuid = null;
        this.tag = null;
        this.yml = null;
        this.file = null;
    }
    public AllianceMember getMember(OfflinePlayer player) {
        final UUID u = player.getUniqueId();
        for(AllianceMember m : members) {
            if(m.uuid.equals(u)) {
                return m;
            }
        }
        return null;
    }
    public void join(Player player) {
        final UUID u = player.getUniqueId();
        final RSPlayer rs = RSPlayer.get(u);
        final Collection<AllianceRole> roles = AllianceRole.paths.values();
        rs.setAlliance(this);
        players.put(u, this);
        members.add(new AllianceMember(System.currentTimeMillis(), u, (AllianceRole) roles.toArray()[roles.size()-1]));
    }
    public void leave(OfflinePlayer player) {
        final UUID u = player.getUniqueId();
        final RSPlayer rs = RSPlayer.get(u);
        rs.setAlliance(null);
        players.remove(u);
        members.remove(getMember(player));
    }
    public void kick(OfflinePlayer player) {
        final UUID u = player.getUniqueId();
        final RSPlayer rs = RSPlayer.get(u);
        rs.setAlliance(null);
        players.remove(u);
        members.remove(getMember(player));
    }


    public AllianceRelationship relationTo(UUID allianceOrPlayer) {
        final HashMap<UUID, Alliance> alliances = Alliance.alliances.containsKey(allianceOrPlayer) ? Alliance.alliances : players.containsKey(allianceOrPlayer) ? players : null;
        final AllianceRelationship re = new AllianceRelationship(0, AllianceRelation.paths.get("neutral"), false);
        if(alliances != null) {
            final Set<UUID> r = relations.keySet();
            final Alliance a = alliances.get(allianceOrPlayer);
            return this == a ? new AllianceRelationship(0, AllianceRelation.paths.get("member"), false) : r.contains(allianceOrPlayer) ? relations.get(allianceOrPlayer) : re;
        }
        return re;
    }
    public AllianceRelation relationTo(Alliance a) {
        final UUID u = a.uuid;
        final HashMap<UUID, AllianceRelationship> r = relations;
        return r.containsKey(u) ? r.get(u).relation : AllianceRelation.paths.get("neutral");
    }
}
