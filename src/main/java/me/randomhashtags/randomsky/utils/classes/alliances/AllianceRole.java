package me.randomhashtags.randomsky.utils.classes.alliances;

import java.util.LinkedHashMap;
import java.util.List;

public class AllianceRole {
    public static LinkedHashMap<String, AllianceRole> paths;
    public final String path, tag, chatTag, color;
    public final List<String> grantedPermissions;
    public AllianceRole(String path, String tag, String chatTag, String color, List<String> grantedPermissions) {
        if(paths == null) {
            paths = new LinkedHashMap<>();
        }
        this.path = path;
        this.tag = tag;
        this.chatTag = chatTag;
        this.color = color;
        this.grantedPermissions = grantedPermissions;
        paths.put(path, this);
    }

    public static void deleteAll() {
        paths = null;
    }
}