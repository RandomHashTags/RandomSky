package me.randomhashtags.randomsky.utils.classes.adventures;

import me.randomhashtags.randomsky.utils.universal.UVersion;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.EntityEquipment;

import java.util.HashMap;
import java.util.List;

public class Bandit {
    private static UVersion uv;
    public static HashMap<String, Bandit> bandits;

    public final String path, type, name;
    public final EntityEquipment equipment;
    public final List<String> attributes;
    public Bandit(String path, String type, String name, EntityEquipment equipment, List<String> attributes) {
        if(bandits == null) {
            bandits = new HashMap<>();
            uv = UVersion.getUVersion();
        }
        this.path = path;
        this.type = type;
        this.name = name;
        this.equipment = equipment;
        this.attributes = attributes;
        bandits.put(path, this);
    }
    public LivingBandit spawn(Location location) {
        final LivingEntity entity = uv.getEntity(type, location, true);
        entity.setCustomName(name);
        final EntityEquipment eq = entity.getEquipment();
        eq.setHelmet(equipment.getHelmet());
        eq.setChestplate(equipment.getChestplate());
        eq.setLeggings(equipment.getLeggings());
        eq.setBoots(equipment.getBoots());
        eq.setItemInMainHand(equipment.getItemInMainHand());
        eq.setItemInOffHand(equipment.getItemInOffHand());
        for(String s : attributes) {
        }
        return new LivingBandit(entity, this);
    }

    public static void deleteAll() {
        bandits = null;
        uv = null;
    }
}
