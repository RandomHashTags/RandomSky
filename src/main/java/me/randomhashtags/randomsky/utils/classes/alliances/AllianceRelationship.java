package me.randomhashtags.randomsky.utils.classes.alliances;

public class AllianceRelationship {
    public long started;
    public AllianceRelation relation;
    public boolean pending;
    public AllianceRelationship(long started, AllianceRelation relation, boolean pending) {
        this.started = started;
        this.relation = relation;
        this.pending = pending;
    }
}
