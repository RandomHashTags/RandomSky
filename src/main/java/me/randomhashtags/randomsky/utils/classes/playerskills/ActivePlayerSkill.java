package me.randomhashtags.randomsky.utils.classes.playerskills;

public class ActivePlayerSkill {
    public final PlayerSkill type;
    public int level;
    public ActivePlayerSkill(PlayerSkill type, int level) {
        this.type = type;
        this.level = level;
    }
}
