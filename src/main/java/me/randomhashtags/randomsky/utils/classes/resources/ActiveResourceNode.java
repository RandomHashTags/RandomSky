package me.randomhashtags.randomsky.utils.classes.resources;

import me.randomhashtags.randomsky.RandomSkyAPI;
import me.randomhashtags.randomsky.api.events.island.HarvestResourceNodeEvent;
import me.randomhashtags.randomsky.utils.classes.island.Island;
import me.randomhashtags.randomsky.utils.universal.UMaterial;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.UUID;

public class ActiveResourceNode {
    private static final RandomSkyAPI api = RandomSkyAPI.getAPI();

    public final UUID uuid;
    public final ResourceNode type;
    public final Island island;
    public final Location location;
    public final World world;
    public long cooldownExpiration;

    public boolean hasPlaced;
    public int respawnTask;

    public ActiveResourceNode(ResourceNode type, Location location) {
        this(UUID.randomUUID(), type, location, System.currentTimeMillis()+type.respawnTime*1000, true);
    }
    public ActiveResourceNode(UUID uuid, ResourceNode type, Location location, long cooldownExpiration, boolean place) {
        this.uuid = uuid;
        this.type = type;
        this.island = Island.valueOf(location);
        this.location = location;
        this.world = location.getWorld();
        this.cooldownExpiration = cooldownExpiration;
        island.activeResourceNodes.add(this);
        this.hasPlaced = place;
        doTask();
        if(place) {
            place();
        } else {
            island.unplacedResourceNodes.add(this);
        }
    }
    public void place() {
        final Block b = world.getBlockAt(location);
        final UMaterial h = type.nodeBlock;
        final ItemStack i = h.getItemStack();
        b.setType(i.getType());
        final BlockState bs = b.getState();
        bs.setRawData(i.getData().getData());
        bs.update();
        hasPlaced = true;
    }
    private void doTask() {
        respawnTask = api.scheduler.scheduleSyncDelayedTask(api.randomsky, () -> {
            final Block b = world.getBlockAt(location);
            final UMaterial h = type.harvestBlock;
            final ItemStack i = h.getItemStack();
            b.setType(i.getType());
            final BlockState bs = b.getState();
            bs.setRawData(i.getData().getData());
            bs.update();
            world.spawnParticle(Particle.BLOCK_CRACK, location.clone().add(0.5, 1, 0.5), 50, i.getData());
        }, 20*type.respawnTime);
    }
    public void delete() {
        Bukkit.getScheduler().cancelTask(respawnTask);
        world.getBlockAt(location).setType(Material.AIR);
        island.activeResourceNodes.remove(this);
    }
    public void harvest(Player player) {
        final Island island = Island.valueOf(location);
        final HarvestResourceNodeEvent e = new HarvestResourceNodeEvent(player, island, this);
        if(!e.isCancelled()) {
            final HashMap<ResourceNode, Integer> mined = island.minedResourceNodes;
            if(!mined.containsKey(type)) mined.put(type, 0);
            mined.put(type, mined.get(type)+1);
            final Location l = location.clone().add(0.5, 1, 0.5);
            for(String s : type.loot) {
                final ItemStack is = api.d(null, s.split(";")[0]);
                final Item item = world.dropItem(l, is);
                item.setPickupDelay(10);
                item.setVelocity(new Vector(0, 0, 0));
            }
            cooldownExpiration = System.currentTimeMillis()+type.respawnTime*1000;
            final Block b = world.getBlockAt(location);
            final UMaterial n = type.nodeBlock;
            b.setType(n.getMaterial());
            final BlockState bs = b.getState();
            bs.setRawData(n.getData());
            bs.update();
            doTask();
        }
    }
}
