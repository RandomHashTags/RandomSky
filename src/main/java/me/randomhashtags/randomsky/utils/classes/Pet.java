package me.randomhashtags.randomsky.utils.classes;

import me.randomhashtags.randomsky.RandomSkyAPI;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.HashMap;

public class Pet {
    public static HashMap<String, Pet> pets;
    private static RandomSkyAPI api;

    private YamlConfiguration yml;
    private String ymlName;
    private ItemStack item;
    private long levelupExp;

    public Pet(File f) {
        if(pets == null) {
            pets = new HashMap<>();
            api = RandomSkyAPI.getAPI();
        }
        yml = YamlConfiguration.loadConfiguration(f);
        ymlName = f.getName().split("\\.yml")[0];
        levelupExp = yml.getLong("settings.levelup exp");
        pets.put(ymlName, this);
    }
    public YamlConfiguration getYaml() { return yml; }
    public String getYamlName() { return ymlName; }

    public long getLevelupExp() { return levelupExp; }

    public ItemStack getItem() {
        if(item == null) item = api.d(yml, "item");
        return item.clone();
    }

    public static void deleteAll() {
        pets = null;
        api = null;
    }
}
