package me.randomhashtags.randomsky.utils.classes.kits;

import me.randomhashtags.randomsky.RandomSkyAPI;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.HashMap;

public class SkyKit {
    public static HashMap<String, SkyKit> kits;
    private static RandomSkyAPI api;
    private YamlConfiguration yml;
    private String ymlName, previewtitle;
    private long cooldown;
    private ItemStack gem, luckyblock;

    public SkyKit(File f) {
        if(kits == null) {
            kits = new HashMap<>();
            api = RandomSkyAPI.getAPI();
        }
        yml = YamlConfiguration.loadConfiguration(f);
        ymlName = f.getName().split("\\.yml")[0];
        cooldown = yml.getLong("settings.cooldown");
        kits.put(ymlName, this);
    }
    public YamlConfiguration getYaml() { return yml; }
    public String getYamlName() { return ymlName; }
    public String getPreviewTitle() {
        if(previewtitle == null) previewtitle = ChatColor.translateAlternateColorCodes('&', yml.getString("settings.preview title"));
        return previewtitle;
    }
    public long getCooldown() { return cooldown; }

    public ItemStack getGem() {
        if(gem == null) gem = api.d(yml, "gem");
        return gem.clone();
    }
    public ItemStack getLuckyBlock() {
        if(luckyblock == null) luckyblock = api.d(yml, "lucky block");
        return luckyblock.clone();
    }

    public static SkyKit valueOfGem(ItemStack gem) {
        if(kits != null && gem != null) {
            for(SkyKit s : kits.values()) {
                if(s.getGem().isSimilar(gem)) {
                    return s;
                }
            }
        }
        return null;
    }
    public static SkyKit valueOfLuckyBlock(ItemStack luckyblock) {
        if(kits != null && luckyblock != null) {
            for(SkyKit s : kits.values()) {
                if(s.getLuckyBlock().isSimilar(luckyblock)) {
                    return s;
                }
            }
        }
        return null;
    }

    public static void deleteAll() {
        kits = null;
        api = null;
    }
}
