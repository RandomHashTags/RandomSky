package me.randomhashtags.randomsky.utils.classes;

import me.randomhashtags.randomsky.utils.universal.UVersion;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RepairScroll {
    public static HashMap<String, RepairScroll> scrolls;
    private static UVersion uversion;
    public final String path;
    public int percentSlot;
    private final ItemStack item;
    public final List<String> applyableTo;
    public RepairScroll(String path, ItemStack item, List<String> applyableTo) {
        if(scrolls == null) {
            scrolls = new HashMap<>();
            uversion = UVersion.getUVersion();
        }
        this.path = path;
        this.item = item;
        final ItemMeta im = item.getItemMeta();
        final List<String> lore = im.getLore();
        for(int i = 0; i < lore.size(); i++) {
            if(lore.get(i).contains("{PERCENT}")) {
                percentSlot = i;
            }
        }
        this.applyableTo = applyableTo;
        scrolls.put(path, this);
    }
    public ItemStack item() { return item.clone(); }

    public static RepairScroll valueOf(ItemStack is) {
        if(scrolls != null && is != null && is.hasItemMeta() && is.getItemMeta().hasDisplayName() && is.getItemMeta().hasLore()) {
            final ItemStack itemstack = is.clone();
            itemstack.setAmount(1);
            final ItemMeta m = itemstack.getItemMeta();
            final List<String> l = m.getLore();
            final String dn = m.getDisplayName();
            final int size = l.size();
            for(String s : scrolls.keySet()) {
                final RepairScroll r = scrolls.get(s);
                final ItemMeta im = scrolls.get(s).item().getItemMeta();
                final List<String> lore = im.getLore();
                if(im.getDisplayName().equals(dn) && lore.size() == size) {
                    if(itemstack.equals(r.get(uversion.getRemainingInt(l.get(r.percentSlot))))) {
                        return r;
                    }
                }
            }
        }
        return null;
    }
    public boolean canBeAppliedTo(ItemStack is) {
        if(is != null) {
            final String t = is.getType().name().toLowerCase();
            for(String s : applyableTo) {
                if(t.endsWith(s.toLowerCase())) {
                    return true;
                }
            }
        }
        return false;
    }
    public ItemStack get(int percent) {
        final ItemStack i = item();
        final ItemMeta im = i.getItemMeta();
        final List<String> l = new ArrayList<>();
        for(String s : im.getLore())
            l.add(s.replace("{PERCENT}", Integer.toString(percent)));
        im.setLore(l);
        i.setItemMeta(im);
        return i;
    }
    public int getPercent(ItemStack item) {
        return item != null && item.hasItemMeta() && item.getItemMeta().hasLore() ? uversion.getRemainingInt(item.getItemMeta().getLore().get(percentSlot)) : 0;
    }
    public static ItemStack get(String path, int percent) {
        if(scrolls != null) {
            final ItemStack i = scrolls.get(path).item();
            final ItemMeta im = i.getItemMeta();
            final List<String> l = new ArrayList<>();
            for(String s : im.getLore())
                l.add(s.replace("{PERCENT}", Integer.toString(percent)));
            im.setLore(l);
            i.setItemMeta(im);
            return i;
        }
        return null;
    }

    public static void deleteAll() {
        scrolls = null;
        uversion = null;
    }
}
