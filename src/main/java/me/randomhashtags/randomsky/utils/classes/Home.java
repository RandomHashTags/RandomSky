package me.randomhashtags.randomsky.utils.classes;

import org.bukkit.Location;

public class Home {
    public String name;
    public Location location;
    public Home(String name, Location location) {
        this.name = name;
        this.location = location;
    }
}
