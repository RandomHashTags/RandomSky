package me.randomhashtags.randomsky.utils.classes.island;

import me.randomhashtags.randomsky.utils.classes.resources.ResourceNode;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class MiningSkill {
    public static HashMap<String, MiningSkill> paths;
    public static HashMap<Integer, MiningSkill> slots;

    public String path;
    public int slot;
    public ResourceNode tracks;
    private ItemStack display;
    public MiningSkill(String path, int slot, ResourceNode tracks, ItemStack display) {
        if(paths == null) {
            paths = new HashMap<>();
            slots = new HashMap<>();
        }
        this.path = path;
        this.slot = slot;
        this.tracks = tracks;
        this.display = display;
        paths.put(path, this);
        slots.put(slot, this);
    }
    public ItemStack display() { return display.clone(); }

    public static void deleteAll() {
        paths = null;
        slots = null;
    }
}
