package me.randomhashtags.randomsky.utils.classes.island;

import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class FarmingSkill {
    public static LinkedHashMap<String, FarmingSkill> paths;
    public static HashMap<Integer, FarmingSkill> slots, levels;

    public final String path, type;
    public final int slot, level, completion;
    private final ItemStack display;
    public final FarmingRecipe requiredRecipe;
    public final FarmingSkill required;
    public FarmingSkill(String path, int level, int slot, int completion, String type, ItemStack display, FarmingSkill required, FarmingRecipe requiredRecipe) {
        if(paths == null) {
            paths = new LinkedHashMap<>();
            slots = new HashMap<>();
            levels = new HashMap<>();
        }
        this.path = path;
        this.level = level;
        this.slot = slot;
        this.completion = completion;
        this.type = type;
        this.display = display;
        this.required = required;
        this.requiredRecipe = requiredRecipe;
        paths.put(path, this);
        slots.put(slot, this);
        levels.put(level, this);
    }
    public ItemStack display() { return display.clone(); }

    public static FarmingSkill valueOf(FarmingSkill required) {
        if(paths != null && required != null) {
            for(FarmingSkill s : paths.values()) {
                if(s.required == required) {
                    return s;
                }
            }
        }
        return null;
    }
    public static FarmingSkill valueOf(String requiredSkill) {
        if(paths != null) {
            for(FarmingSkill f : paths.values()) {
                if(f.required != null && f.required.path.equals(requiredSkill)) {
                    return f;
                }
            }
        }
        return null;
    }
    public static FarmingSkill valueOf(FarmingRecipe required) {
        if(paths != null) {
            for(FarmingSkill f : paths.values()) {
                if(f.requiredRecipe == required) {
                    return f;
                }
            }
        }
        return null;
    }
    public static void deleteAll() {
        paths = null;
        slots = null;
        levels = null;
    }
}
