package me.randomhashtags.randomsky.utils.classes.playerskills;

import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PlayerSkill {
    public static HashMap<String, PlayerSkill> paths;
    public static HashMap<Integer, PlayerSkill> slots;

    public final String path;
    public final int maxLevel, slot;
    private final ItemStack display;
    public List<PlayerSkillLevel> levels = new ArrayList<>();
    public PlayerSkill(String path, int maxLevel, int slot, ItemStack display) {
        if(paths == null) {
            paths = new HashMap<>();
            slots = new HashMap<>();
        }
        this.path = path;
        this.maxLevel = maxLevel;
        this.slot = slot;
        this.display = display;
        paths.put(path, this);
        slots.put(slot, this);
    }
    public ItemStack display() {
        if(display == null) {
        }
        return display.clone();
    }

    public static void deleteAll() {
        paths = null;
        slots = null;
    }
}
