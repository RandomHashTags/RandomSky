package me.randomhashtags.randomsky.utils.classes.chat;

import java.util.ArrayList;
import java.util.List;

public class ChatChannels {
    public boolean global, island, alliance, ally, truce, local;
    public ChatChannel current;
    public List<ChatChannel> active = new ArrayList<>();
    public ChatChannels(ChatChannel current, boolean global, boolean island, boolean alliance, boolean ally, boolean truce, boolean local) {
        this.current = current;
        this.global = global;
        this.island = island;
        this.alliance = alliance;
        this.ally = ally;
        this.truce = truce;
        this.local = local;
        if(global) active.add(ChatChannel.GLOBAL);
        if(island) active.add(ChatChannel.ISLAND);
        if(alliance) active.add(ChatChannel.ALLIANCE);
        if(ally) active.add(ChatChannel.ALLY);
        if(truce) active.add(ChatChannel.TRUCE);
        if(local) active.add(ChatChannel.LOCAL);
    }
}
