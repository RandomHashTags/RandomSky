package me.randomhashtags.randomsky.utils.classes.adventures;

import me.randomhashtags.randomsky.RandomSkyAPI;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class Adventure {
    public static HashMap<String, Adventure> adventures;
    private static RandomSkyAPI api;
    private static Random random;

    private File f;
    private YamlConfiguration yml;
    private String ymlName, name;
    public final int maxPlayers, slot, teleportDelay;
    private ItemStack display;
    private List<String> blacklistedItems;
    private List<Location> teleportLocations;
    private ItemStack map, fragment;
    public Adventure mapFoundIn;
    private List<Player> online;
    private List<Location> chests;
    public Adventure(File f) {
        if(adventures == null) {
            adventures = new HashMap<>();
            api = RandomSkyAPI.getAPI();
            random = api.random;
        }
        this.f = f;
        this.yml = YamlConfiguration.loadConfiguration(f);
        ymlName = f.getName().split("\\.yml")[0];
        maxPlayers = yml.getInt("max players");
        teleportDelay = yml.getInt("teleport delay");
        slot = yml.getInt("gui.slot");
        adventures.put(ymlName, this);
    }
    public YamlConfiguration getYaml() { return yml; }
    public String getYamlName() { return ymlName; }
    public String getName() {
        if(name == null) name = ChatColor.translateAlternateColorCodes('&', yml.getString("name"));
        return name;
    }
    public ItemStack display() {
        if(display == null) display = api.d(yml, "gui");
        return display.clone();
    }

    public List<String> getBlacklistedItems() {
        if(blacklistedItems == null) blacklistedItems = yml.getStringList("blacklisted items");
        return blacklistedItems;
    }

    public List<Location> getTeleportLocations() {
        if(teleportLocations == null) {
            teleportLocations = new ArrayList<>();
            for(String s : yml.getStringList("teleport locations")) {
                teleportLocations.add(api.toLocation(s));
            }
        }
        return teleportLocations;
    }
    public ItemStack getMap() {
        if(map == null && yml.get("map") != null) map = api.d(yml, "map");
        return map.clone();
    }
    public ItemStack getFragment() {
        if(fragment == null && yml.get("map.fragment") != null) fragment = api.d(yml, "map.fragment");
        return fragment.clone();
    }

    public List<Player> getPlayers() {
        if(online == null) online = new ArrayList<>();
        return online;
    }
    public void join(Player player) {
        if(online == null) online = new ArrayList<>();
        player.teleport(getTeleportLocations().get(random.nextInt(teleportLocations.size())), PlayerTeleportEvent.TeleportCause.PLUGIN);
        online.add(player);
        api.sendStringListMessage(player, yml.getStringList("messages.teleport"), null);
    }
    public void leave(Player player, boolean kick) {
        if(kick) player.teleport(player, PlayerTeleportEvent.TeleportCause.PLUGIN);
        online.remove(player);
    }
    public List<Location> getChests() {
        if(chests == null) {
            chests = new ArrayList<>();
            for(String s : yml.getStringList("chests")) {
                chests.add(api.toLocation(s));
            }
        }
        return chests;
    }
    public void addChestLocation(Location l) {
        if(!chests.contains(l)) chests.add(l);
    }
    public void removeChestLocation(Location l) {
        chests.remove(l);
    }

    public void backup() {
        final List<String> chestL = new ArrayList<>(), tl = new ArrayList<>();
        for(Location l : getChests()) chestL.add(api.toString(l));
        for(Location l : getTeleportLocations()) tl.add(api.toString(l));

        yml.set("teleport locations", tl);
        yml.set("chests", chestL);

        save();
    }
    public void save() {
        try {
            yml.save(f);
            yml = YamlConfiguration.loadConfiguration(f);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static Adventure valueOf(int slot) {
        if(adventures != null) {
            for(Adventure a : adventures.values())
                if(a.slot == slot)
                    return a;
        }
        return null;
    }
    public static Adventure valueOfMap(ItemStack map) {
        if(adventures != null && map != null) {
            for(Adventure a : adventures.values()) {
                if(a.getMap().isSimilar(map)) {
                    return a;
                }
            }
        }
        return null;
    }
    public static Adventure valueOfFragment(ItemStack fragment) {
        if(adventures != null && fragment != null) {
            for(Adventure a : adventures.values()) {
                if(a.getFragment().isSimilar(fragment)) {
                    return a;
                }
            }
        }
        return null;
    }

    public static void deleteAll() {
        if(adventures != null) {
            for(Adventure a : adventures.values()) {
                for(Player player : a.online) {
                    a.leave(player, true);
                }
            }
        }
        adventures = null;
        api = null;
        random = null;
    }
}
