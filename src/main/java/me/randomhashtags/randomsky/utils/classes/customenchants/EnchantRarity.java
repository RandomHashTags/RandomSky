package me.randomhashtags.randomsky.utils.classes.customenchants;

import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class EnchantRarity {
    public static final List<EnchantRarity> rarities = new ArrayList<>();

    public final String path;
    private final ItemStack display;
    public final int minBreakChance, maxBreakChance;
    public EnchantRarity(String path, ItemStack display, int minBreakChance, int maxBreakChance) {
        this.path = path;
        this.display = display;
        this.minBreakChance = minBreakChance;
        this.maxBreakChance = maxBreakChance;
        rarities.add(this);
    }
    public ItemStack display() { return display.clone(); }
}
