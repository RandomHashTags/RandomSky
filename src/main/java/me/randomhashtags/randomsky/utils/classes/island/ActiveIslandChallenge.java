package me.randomhashtags.randomsky.utils.classes.island;

public class ActiveIslandChallenge {

    public final IslandChallenge type;
    public double progress;
    public ActiveIslandChallenge(IslandChallenge type, double progress) {
        this.type = type;
        this.progress = progress;
    }
}
