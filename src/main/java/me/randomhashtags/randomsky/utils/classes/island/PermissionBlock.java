package me.randomhashtags.randomsky.utils.classes.island;

import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class PermissionBlock {
    public static HashMap<String, PermissionBlock> paths;

    public final String path;
    private final ItemStack item;
    public final int radius;
    public PermissionBlock(String path, ItemStack item, int radius) {
        if(paths == null) {
            paths = new HashMap<>();
        }
        this.path = path;
        this.item = item;
        this.radius = radius;
        paths.put(path, this);
    }
    public ItemStack item() { return item.clone(); }

    public static PermissionBlock valueOf(ItemStack is) {
        if(paths != null && is != null) {
            for(PermissionBlock p : paths.values()) {
                if(p.item().isSimilar(is)) {
                    return p;
                }
            }
        }
        return null;
    }
    public static void deleteAll() {
        paths = null;
    }
}
