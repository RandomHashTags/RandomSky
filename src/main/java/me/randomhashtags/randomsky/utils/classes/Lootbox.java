package me.randomhashtags.randomsky.utils.classes;

import me.randomhashtags.randomsky.RandomSkyAPI;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Lootbox {

    public static List<Lootbox> lootboxes;
    private static RandomSkyAPI api;
    private static Random random;

    public YamlConfiguration yml;
    public String ymlName, name, guiTitle, previewTitle, regularLootSize, bonusLootSize;
    public int priority, availableFor, guiSize;
    public List<String> guiFormat, regularLootFormat, jackpotLootFormat, bonusLootFormat, randomLoot, jackpotLoot, bonusLoot;
    private ItemStack item, background;

    public Lootbox(YamlConfiguration yml, String ymlName) {
        if(lootboxes == null) {
            lootboxes = new ArrayList<>();
            api = RandomSkyAPI.getAPI();
            random = new Random();
        }
        this.yml = yml;
        this.ymlName = ymlName;
        this.priority = yml.getInt("priority");
        this.name = ChatColor.translateAlternateColorCodes('&', yml.getString("name"));
        this.availableFor = yml.getInt("available for");

        this.background = api.d(yml, "gui.background");
        this.guiTitle = ChatColor.translateAlternateColorCodes('&', yml.getString("gui.title"));
        this.guiSize = yml.getInt("gui.size");
        this.guiFormat = yml.getStringList("gui.format");

        this.previewTitle = ChatColor.translateAlternateColorCodes('&', yml.getString("preview title"));
        this.regularLootFormat = api.colorizeListString(yml.getStringList("lore formats.regular loot"));
        this.jackpotLootFormat = api.colorizeListString(yml.getStringList("lore formats.jackpot loot"));
        this.bonusLootFormat = api.colorizeListString(yml.getStringList("lore formats.bonus loot"));
        this.regularLootSize = yml.getString("regular loot size");
        this.bonusLootSize = yml.getString("bonus loot size");
        this.randomLoot = yml.getStringList("regular loot");
        this.jackpotLoot = yml.getStringList("jackpot loot");
        this.bonusLoot = yml.getStringList("bonus loot");
        final ItemStack i = api.d(yml, "lootbox");
        final ItemMeta itemMeta = i.getItemMeta();
        final List<String> lore = new ArrayList<>();
        for(String s : itemMeta.getLore()) {
            if(s.equals("{REGULAR_LOOT}")) {
                for(ItemStack is : regularLoot()) {
                    for(String z : regularLootFormat) {
                        final String it = is.hasItemMeta() && is.getItemMeta().hasDisplayName() ? is.getItemMeta().getDisplayName() : api.toMaterial(is.getType().name(), false);
                        lore.add(z.replace("{AMOUNT}", Integer.toString(is.getAmount())).replace("{ITEM}", it));
                    }
                }
            } else if(s.equals("{JACKPOT_LOOT}")) {
                for(ItemStack is : jackpotLoot()) {
                    for(String z : jackpotLootFormat) {
                        final String it = is.hasItemMeta() && is.getItemMeta().hasDisplayName() ? is.getItemMeta().getDisplayName() : api.toMaterial(is.getType().name(), false);
                        lore.add(z.replace("{AMOUNT}", Integer.toString(is.getAmount())).replace("{ITEM}", it));
                    }
                }
            } else if(s.equals("{BONUS_LOOT}")) {
                for(ItemStack is : bonusLoot()) {
                    for(String z : bonusLootFormat) {
                        final String it = is.hasItemMeta() && is.getItemMeta().hasDisplayName() ? is.getItemMeta().getDisplayName() : api.toMaterial(is.getType().name(), false);
                        lore.add(z.replace("{AMOUNT}", Integer.toString(is.getAmount())).replace("{ITEM}", it));
                    }
                }
            } else {
                lore.add(s);
            }
        }
        itemMeta.setLore(lore);
        i.setItemMeta(itemMeta);
        this.item = i;
        lootboxes.add(this);
    }
    public ItemStack item() { return item.clone(); }
    public ItemStack background() { return background.clone(); }
    public int randomRegularLootSize() {
        final int min = Integer.parseInt(regularLootSize.contains("-") ? regularLootSize.split("-")[0] : regularLootSize), max = maxRegularLoot();
        return regularLootSize.contains("-") ? min+random.nextInt(max-min+1) : min;
    }
    public int maxRegularLoot() {
        return regularLootSize.contains("-") ? Integer.parseInt(regularLootSize.split("-")[1]) : Integer.parseInt(regularLootSize);
    }

    public List<ItemStack> regularLoot() {
        final List<ItemStack> items = new ArrayList<>();
        for(String s : randomLoot) items.add(api.d(null, s));
        return items;
    }
    public String randomRegularLoot(List<String> excluding) {
        final List<String> loot = new ArrayList<>(randomLoot);
        loot.addAll(jackpotLoot);
        for(String s : excluding) {
            loot.remove(s);
        }
        return loot.get(random.nextInt(loot.size()));
    }
    public List<ItemStack> jackpotLoot() {
        final List<ItemStack> items = new ArrayList<>();
        for(String s : jackpotLoot) items.add(api.d(null, s));
        return items;
    }
    public String randomBonusLoot(List<String> excluding) {
        final List<String> loot = new ArrayList<>(bonusLoot);
        for(String s : excluding) {
            loot.remove(s);
        }
        final int s = loot.size();
        return s > 0 ? loot.get(random.nextInt(s)) : "air";
    }
    public List<ItemStack> bonusLoot() {
        final List<ItemStack> items = new ArrayList<>();
        for(String s : bonusLoot) items.add(api.d(null, s));
        return items;
    }
    public List<ItemStack> items() {
        final List<ItemStack> items = new ArrayList<>();
        for(String s : randomLoot) items.add(api.d(null, s));
        for(String s : jackpotLoot) items.add(api.d(null, s));
        for(String s : bonusLoot) items.add(api.d(null, s));
        return items;
    }


    public static Lootbox valueOf(String guiTitle) {
        if(lootboxes != null) {
            for(Lootbox l : lootboxes)
                if(l.guiTitle.equals(guiTitle))
                    return l;
        }
        return null;
    }
    public static Lootbox valueof(String previewTitle) {
        if(lootboxes != null) {
            for(Lootbox l : lootboxes)
                if(l.previewTitle.equals(previewTitle))
                    return l;
        }
        return null;
    }
    public static Lootbox valueOF(String fileName) {
        if(lootboxes != null) {
            for(Lootbox l : lootboxes)
                if(l.ymlName.equals(fileName))
                    return l;
        }
        return null;
    }
    public static Lootbox valueOf(ItemStack is) {
        if(lootboxes != null && is != null && is.hasItemMeta())
            for(Lootbox l : lootboxes)
                if(l.item().isSimilar(is))
                    return l;
        return null;
    }
    public static Lootbox valueOf(int priority) {
        if(lootboxes != null) {
            for(Lootbox l : lootboxes)
                if(l.priority == priority)
                    return l;
        }
        return null;
    }
    public static Lootbox latest() {
        int p = 0;
        Lootbox lo = null;
        if(lootboxes != null) {
            for(Lootbox l : lootboxes) {
                if(lo == null || l.priority > p) {
                    p = l.priority;
                    lo = l;
                }
            }
        }
        return lo;
    }
    public static void deleteAll() {
        lootboxes = null;
        api = null;
        random = null;
    }
}
