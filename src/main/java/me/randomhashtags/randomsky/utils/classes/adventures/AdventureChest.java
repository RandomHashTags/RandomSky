package me.randomhashtags.randomsky.utils.classes.adventures;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class AdventureChest {
    public static final HashMap<String, AdventureChest> types = new HashMap<>();
    private static final Random random = new Random();

    public final String path, amount;
    public final int radius;
    public final List<String> loot;
    public AdventureChest(String path, int radius, String amount, List<String> loot) {
        this.path = path;
        this.radius = radius;
        this.amount = amount;
        this.loot = loot;
        types.put(path, this);
    }

    public int getAmount() {
        final int min = amount.contains("-") ? Integer.parseInt(amount.split("-")[0]) : Integer.parseInt(amount), max = amount.contains("-") ? Integer.parseInt(amount.split("-")[1]) : -1;
        return max == -1 ? min : min+random.nextInt(max-min+1);
    }
}
