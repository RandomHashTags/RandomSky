package me.randomhashtags.randomsky.utils.classes.island;

import me.randomhashtags.randomsky.api.IslandFarming;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class FarmingLimitIncrease {
    public static HashMap<String, FarmingLimitIncrease> limits;
    private static IslandFarming api;
    private String path;
    private ItemStack item;
    public FarmingLimitIncrease(String path) {
        if(limits == null) {
            limits = new HashMap<>();
            api = IslandFarming.getIslandFarming();
        }
        this.path = path;
        limits.put(path, this);
    }
    public String getPath() { return path; }
    public ItemStack getItem() {
        if(item == null) item = api.d(api.config, "limit increase." + path);
        return item.clone();
    }

    public static void deleteAll() {
        limits = null;
        api = null;
    }
}
