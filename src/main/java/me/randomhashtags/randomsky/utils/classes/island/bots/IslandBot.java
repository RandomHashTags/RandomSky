package me.randomhashtags.randomsky.utils.classes.island.bots;

import java.util.ArrayList;
import java.util.List;

public class IslandBot {
    public static List<IslandBot> bots;

    public final String path;
    public IslandBot(String path) {
        if(bots == null) {
            bots = new ArrayList<>();
        }
        this.path = path;
        bots.add(this);
    }
    public static void deleteAll() {
        bots = null;
    }
}
