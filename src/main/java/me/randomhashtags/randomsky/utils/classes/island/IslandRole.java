package me.randomhashtags.randomsky.utils.classes.island;

import java.util.HashMap;
import java.util.List;

public class IslandRole {
    public static HashMap<String, IslandRole> paths;
    public static IslandRole defaultMember = null, defaultCreator = null;

    public final String path, rank, name;
    public final List<String> lore, addedPermissions;
    public IslandRole(String path, String rank, String name, List<String> lore, List<String> addedPermissions) {
        if(paths == null) {
            paths = new HashMap<>();
        }
        this.path = path;
        this.rank = rank;
        this.name = name;
        this.lore = lore;
        this.addedPermissions = addedPermissions;
        paths.put(path, this);
    }
    public static void deleteAll() {
        paths = null;
        defaultMember = null;
        defaultCreator = null;
    }
}
