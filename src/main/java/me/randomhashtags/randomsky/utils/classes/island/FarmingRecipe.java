package me.randomhashtags.randomsky.utils.classes.island;

import me.randomhashtags.randomsky.utils.universal.UMaterial;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class FarmingRecipe {
    public static List<FarmingRecipe> defaults = new ArrayList<>();
    public static LinkedHashMap<String, FarmingRecipe> paths;

    public final String path, recipeName;
    public final UMaterial unlocks;
    private final ItemStack item;
    public FarmingRecipe(String path, String recipeName, UMaterial unlocks, ItemStack item) {
        if(paths == null) {
            paths = new LinkedHashMap<>();
        }
        this.path = path;
        this.recipeName = recipeName;
        this.unlocks = unlocks;
        this.item = item;
        paths.put(path, this);
    }
    public ItemStack item() { return item.clone(); }

    public static FarmingRecipe valueOf(ItemStack i) {
        if(i != null && i.hasItemMeta() && i.getItemMeta().hasLore()) {
            if(defaults != null) {
                for(FarmingRecipe f : defaults)
                    if(f.item().isSimilar(i))
                        return f;
            }
            if(paths != null) {
                for(FarmingRecipe f : paths.values())
                    if(f.item().isSimilar(i))
                        return f;
            }
        }
        return null;
    }
    public static FarmingRecipe valueOfSeed(UMaterial seed) {
        if(paths != null && seed != null) {
            for(FarmingRecipe r : paths.values()) {
                if(r.unlocks == seed) {
                    return r;
                }
            }
        }
        return null;
    }
    public static FarmingRecipe valueOf(String path) {
        return paths != null ? paths.getOrDefault(path, null) : null;
    }
    public static void deleteAll() {
        defaults = null;
        paths = null;
    }
}
