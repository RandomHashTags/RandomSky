package me.randomhashtags.randomsky.utils.classes;

import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;

public class PlayerRank {
    public static HashMap<String, PlayerRank> paths;
    public final int priority;
    public final String path, appearance;
    private final ItemStack item;
    public final List<String> attributes;
    public PlayerRank(String path, String appearance, ItemStack item, List<String> attributes) {
        if(paths == null) {
            paths = new HashMap<>();
        }
        this.priority = paths.size();
        this.path = path;
        this.appearance = appearance;
        this.item = item;
        this.attributes = attributes;
        paths.put(path, this);
    }
    public ItemStack item() { return item.clone(); }

    public static PlayerRank valueOf(ItemStack itemstack) {
        if(paths != null && itemstack != null && itemstack.hasItemMeta()) {
            final ItemStack  i = itemstack.clone(); i.setAmount(1);
            for(PlayerRank r : paths.values())
                if(i.isSimilar(r.item()))
                    return r;
        }
        return null;
    }
    public static void deleteAll() {
        paths = null;
    }
}
