package me.randomhashtags.randomsky.utils.classes.island;

import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class SlayerSkill {
    public static HashMap<String, SlayerSkill> paths;
    public static HashMap<Integer, SlayerSkill> slots;

    public final String path, entity, type;
    public final int slot, level, completion;
    public final ItemStack display;
    public final SlayerSkill required;
    public SlayerSkill(String path, int level, int slot, String entity, int completion, String type, ItemStack display, SlayerSkill required) {
        if(paths == null) {
            paths = new HashMap<>();
            slots = new HashMap<>();
        }
        this.path = path;
        this.level = level;
        this.slot = slot;
        this.entity = entity;
        this.completion = completion;
        this.type = type;
        this.display = display;
        this.required = required;
        paths.put(path, this);
        slots.put(slot, this);
    }

    public static SlayerSkill valueOf(String path) {
        return paths != null ? paths.getOrDefault(path, null) : null;
    }
}
