package me.randomhashtags.randomsky.utils.classes.customenchants;

import java.util.ArrayList;
import java.util.List;

public class CustomEnchant {
    public static final List<CustomEnchant> enabledEnchants = new ArrayList<>(), disabledEnchants = new ArrayList<>();

    public final String path, name;
    public final boolean enabled;
    public final int maxLevel;
    public final EnchantRarity rarity;
    public CustomEnchant(String path, boolean enabled, String name, int maxLevel, EnchantRarity rarity) {
        this.path = path;
        this.enabled = enabled;
        this.name = name;
        this.maxLevel = maxLevel;
        this.rarity = rarity;
        (enabled ? enabledEnchants : disabledEnchants).add(this);
    }
}
