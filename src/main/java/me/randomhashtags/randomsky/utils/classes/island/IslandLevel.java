package me.randomhashtags.randomsky.utils.classes.island;

import java.util.HashMap;
import java.util.List;

public class IslandLevel {
    public static HashMap<String, IslandLevel> paths;
    public static HashMap<Integer, IslandLevel> slots, levels;

    public String path;
    public int slot, level;
    public List<String> cost;
    public List<String> rewards;
    public IslandLevel required;
    public IslandLevel(String path, int slot, int level, List<String> cost, List<String> rewards, IslandLevel required) {
        if(paths == null) {
            paths = new HashMap<>();
            slots = new HashMap<>();
            levels = new HashMap<>();
        }
        this.path = path;
        this.slot = slot;
        this.level = level;
        this.cost = cost;
        this.rewards = rewards;
        this.required = required;
        paths.put(path, this);
        slots.put(slot, this);
        levels.put(level, this);
    }

    public static void deleteAll() {
        paths = null;
        slots = null;
        levels = null;
    }
}
