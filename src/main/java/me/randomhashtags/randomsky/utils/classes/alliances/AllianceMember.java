package me.randomhashtags.randomsky.utils.classes.alliances;

import java.util.UUID;

public class AllianceMember {
    public final long joined;
    public final UUID uuid;
    public AllianceRole role;
    public AllianceMember(long joined, UUID uuid, AllianceRole role) {
        this.joined = joined;
        this.uuid = uuid;
        this.role = role;
    }
}