package me.randomhashtags.randomsky.utils.classes.mobstacker;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;

import java.util.HashMap;
import java.util.UUID;

public class StackedEntity {
    public static HashMap<UUID, StackedEntity> stacked;

    public final long creationTime;
    public UUID uuid;
    public LivingEntity base;
    public final String name;
    private EntityType f;
    public int stackSize;

    public StackedEntity(long creationTime, String name, LivingEntity base, int stackSize) {
        if(stacked == null) {
            stacked = new HashMap<>();
        }
        this.creationTime = creationTime;
        this.name = name;
        this.base = base;
        this.uuid = base.getUniqueId();
        this.stackSize = stackSize;
        this.f = base.getType();
        stacked.put(uuid, this);
    }
    private void delete() {
        stacked.remove(uuid);
        base.remove();
        this.uuid = null;
        this.base = null;
        this.f = null;
        this.stackSize = 0;
        if(stacked.isEmpty()) {
            stacked = null;
        }
    }

    public void merge(LivingEntity entity) {
        if(stacked != null && entity.getType().equals(f)) {
            final UUID e = entity.getUniqueId();
            final StackedEntity u = stacked.getOrDefault(e, null);
            if(u != null) {
                stackSize += u.stackSize;
                u.delete();
            } else {
                stackSize += 1;
                entity.remove();
            }
            base.setCustomName(name.replace("{SIZE}", Integer.toString(stackSize)));
        }
    }
    public static void deleteAll() {
        stacked = null;
    }
}
