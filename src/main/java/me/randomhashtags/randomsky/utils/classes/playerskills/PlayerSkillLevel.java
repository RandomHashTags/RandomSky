package me.randomhashtags.randomsky.utils.classes.playerskills;

import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class PlayerSkillLevel {
    public static List<PlayerSkillLevel> levels;
    public PlayerSkill parent;
    public final String path, perk;
    public final int level, slot;
    private final ItemStack display;
    public final List<String> attributes;
    public PlayerSkillLevel(PlayerSkill parent, String path, int level, int slot, ItemStack display, String perk, List<String> attributes) {
        if(levels == null) {
            levels = new ArrayList<>();
        }
        this.parent = parent;
        this.path = path;
        this.level = level;
        this.slot = slot;
        this.display = display;
        this.perk = perk;
        this.attributes = attributes;
        levels.add(this);
    }
    public ItemStack display() { return display.clone(); }

    public static PlayerSkillLevel valueOf(PlayerSkill parent, int slot) {
        if(levels != null) {
            for(PlayerSkillLevel p : levels)
                if(p.parent.equals(parent) && p.slot == slot)
                    return p;
        }
        return null;
    }
    public static void deleteAll() {
        levels = null;
    }
}
