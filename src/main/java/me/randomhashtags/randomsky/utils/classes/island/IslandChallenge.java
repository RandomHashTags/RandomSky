package me.randomhashtags.randomsky.utils.classes.island;

import me.randomhashtags.randomsky.utils.universal.UVersion;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

public class IslandChallenge {
    public static LinkedHashMap<String, IslandChallenge> paths;
    public static HashMap<Integer, IslandChallenge> slots;

    public final String path, name;
    public final int slot;
    public final double completion;
    public final List<String> objective, rewards, attributes;

    public IslandChallenge(String path, int slot, String name, double completion, List<String> objective, List<String> rewards, List<String> attributes) {
        if(paths == null) {
            paths = new LinkedHashMap<>();
            slots = new HashMap<>();
        }
        this.path = path;
        this.slot = slot;
        this.name = name;
        this.completion = completion;
        this.objective = objective;
        this.rewards = rewards;
        this.attributes = attributes;
        paths.put(path, this);
        slots.put(slot, this);
    }

    public static IslandChallenge getNextLevel(IslandChallenge current) {
        final String c = current != null ? current.path : null;
        final Set<String> key = paths.keySet();
        final int index = c != null ? UVersion.getUVersion().indexOf(key, c) : -1;
        return index != -1 && index < key.size() ? paths.get(key.toArray()[index+1].toString()) : null;
    }
    public static void deleteAll() {
        paths = null;
        slots = null;
    }
}
