package me.randomhashtags.randomsky.utils.classes.island.bots;

import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class LivingIslandBot {
    public static List<LivingIslandBot> bots;

    public final UUID uuid;
    public final long creationTime;
    public final UUID placer;
    public final Location location;
    public final IslandBot type;
    public LivingIslandBot(UUID uuid, long creationTime, UUID placer, Location location, IslandBot type) {
        if(bots == null) {
            bots = new ArrayList<>();
        }
        this.uuid = uuid;
        this.creationTime = creationTime;
        this.placer = placer;
        this.location = location;
        this.type = type;
        bots.add(this);
    }
    public static void deleteAll() {
        bots = null;
    }
}
