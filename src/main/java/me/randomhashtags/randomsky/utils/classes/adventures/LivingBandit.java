package me.randomhashtags.randomsky.utils.classes.adventures;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class LivingBandit {
    public static final List<LivingBandit> living = new ArrayList<>();

    public final LivingEntity entity;
    public final Bandit type;
    public LivingBandit(LivingEntity entity, Bandit type) {
        this.entity = entity;
        this.type = type;
        living.add(this);
    }
    public void kill(Entity killer) {
        living.remove(this);
        for(String s : type.attributes) {
        }
    }

    public static LivingBandit valueOf(UUID uuid) {
        if(uuid != null)
            for(LivingBandit l : living)
                if(l.entity.getUniqueId().equals(uuid))
                    return l;
        return null;
    }
}
