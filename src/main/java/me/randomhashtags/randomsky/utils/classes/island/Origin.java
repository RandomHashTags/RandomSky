package me.randomhashtags.randomsky.utils.classes.island;

import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.HashMap;
import java.util.List;

public class Origin {
    public static HashMap<String, Origin> paths;
    public String path, string;
    public File schematic;
    public int slot;
    private ItemStack display;
    public List<String> perks;
    public Origin(String path, File schematic, String string, int slot, ItemStack display, List<String> perks) {
        if(paths == null) {
            paths = new HashMap<>();
        }
        this.path = path;
        this.schematic = schematic;
        this.string = string;
        this.slot = slot;
        this.display = display;
        this.perks = perks;
        paths.put(path, this);
    }
    public ItemStack display() { return display.clone(); }

    public static Origin valueOf(int slot) {
        for(Origin o : paths.values())
            if(o.slot == slot)
                return o;
        return null;
    }
    public static void deleteAll() {
        paths = null;
    }
}
