package me.randomhashtags.randomsky.utils.classes.island;

import me.randomhashtags.randomsky.utils.classes.PolyBoundary;
import org.bukkit.Location;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ActivePermissionBlock {
    private UUID uuid;
    public List<UUID> members = new ArrayList<>();
    private Location location;
    private PolyBoundary boundary;
    private PermissionBlock type;
    public boolean interact, interactWithDoors, interactWithLevers, interactWithPressurePlates, interactWithHoppers, interactWithChests, interactWithEntities, damageEntities, pickupExp, pickupItems, dropItems, placeBlocks, breakBlocks, harvestResourceNodes, publicRegion, safePVP;
    public ActivePermissionBlock(Location location, PermissionBlock type) {
        this(UUID.randomUUID(), location, type, new ArrayList<>(), false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false);
    }
    public ActivePermissionBlock(UUID uuid, Location location, PermissionBlock type, List<UUID> members, boolean interact, boolean interactWithDoors, boolean interactWithLevers, boolean interactWithPressurePlates, boolean interactWithHoppers, boolean interactWithChests, boolean interactWithEntities, boolean damageEntities, boolean pickupExp, boolean pickupItems, boolean dropItems, boolean placeBlocks, boolean breakBlocks, boolean harvestResourceNodes, boolean publicRegion, boolean safePVP) {
        this.uuid = uuid;
        this.location = location;
        this.type = type;
        this.members.addAll(members);
        this.interact = interact;
        this.interactWithDoors = interactWithDoors;
        this.interactWithLevers = interactWithLevers;
        this.interactWithPressurePlates = interactWithPressurePlates;
        this.interactWithHoppers = interactWithHoppers;
        this.interactWithChests = interactWithChests;
        this.interactWithEntities = interactWithEntities;
        this.damageEntities = damageEntities;
        this.pickupExp = pickupExp;
        this.pickupItems = pickupItems;
        this.dropItems = dropItems;
        this.placeBlocks = placeBlocks;
        this.breakBlocks = breakBlocks;
        this.harvestResourceNodes = harvestResourceNodes;
        this.publicRegion = publicRegion;
        this.safePVP = safePVP;
        this.boundary = new PolyBoundary(location, type.radius-1);
    }
    public void delete() {
        this.uuid = null;
        this.location.getWorld().getBlockAt(location).setType(Material.AIR);
        this.members = null;
        this.location = null;
        this.type = null;
        this.boundary = null;
    }
    public UUID getUUID() { return uuid; }
    public PermissionBlock getType() { return type; }
    public Location getLocation() { return location; }
    public PolyBoundary getBoundary() { return boundary; }
}
