package me.randomhashtags.randomsky.utils.classes.island;

import me.randomhashtags.randomsky.api.ItemRecipes;
import me.randomhashtags.randomsky.utils.universal.UMaterial;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class ItemRecipe {
    public static HashMap<String, ItemRecipe> recipes;
    private static ItemRecipes api;
    private String path;
    private ItemStack page, item;
    private UMaterial unlocks;
    public ItemRecipe(String path) {
        if(recipes == null) {
            recipes = new HashMap<>();
            api = ItemRecipes.getItemRecipes();
        }
        this.path = path;
        recipes.put(path, this);
    }
    public String getPath() { return path; }
    public ItemStack getItem() {
        if(item == null) item = api.d(api.config, "recipes." + path);
        return item.clone();
    }
    public ItemStack getPage() {
        if(page == null) page = api.d(api.config, "recipes." + path + ".page");
        return page.clone();
    }

    public static void deleteAll() {
        recipes = null;
        api = null;
    }
}
