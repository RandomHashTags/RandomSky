package me.randomhashtags.randomsky.utils.classes.island;

import me.randomhashtags.randomsky.RandomSky;
import me.randomhashtags.randomsky.api.Islands;
import me.randomhashtags.randomsky.utils.RSPlayer;
import me.randomhashtags.randomsky.utils.enums.InviteType;
import me.randomhashtags.randomsky.utils.classes.PolyBoundary;
import me.randomhashtags.randomsky.utils.classes.RSInvite;
import me.randomhashtags.randomsky.utils.classes.resources.ActiveResourceNode;
import me.randomhashtags.randomsky.utils.classes.resources.ResourceNode;
import me.randomhashtags.randomsky.utils.universal.UVersion;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitScheduler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class Island {
    private static final String s = File.separator, folder = RandomSky.getPlugin.getDataFolder() + s + "_Data" + s + "islands";
    private static final UVersion uv = UVersion.getUVersion();
    public static final HashMap<UUID, Island> islands = new HashMap<>(), players = new HashMap<>();

    private boolean isLoaded = false, openToPublic;
    private UUID uuid, creator;
    private File file;
    private YamlConfiguration yml;
    private long creationTime;
    private Origin origin;

    private Location center, home, warp;
    public IslandLevel level;
    public PolyBoundary boundary, maxBoundary;
    public String tag, description;
    public int radius, spawnerLimit, maxMembers;
    public double XPGainMultiplier = 1.00, sellPriceMultiplier = 1.00, outgoingPvEDamageMultiplier = 1.00, incomingPvPDamageMultiplier = 1.00, outgoingPvPDamageMultiplier = 1.00;

    public List<PotionEffectType> immuneTo;
    public List<UUID> banned;
    public List<RSInvite> invites;
    public List<FarmingRecipe> allowedCrops;
    public List<ResourceNode> allowedNodes;
    public List<String> allowedMobs;
    public List<ActivePermissionBlock> permissionBlocks;
    public List<ActiveResourceNode> activeResourceNodes, unplacedResourceNodes;

    public ActiveIslandSkill farmingSkill, miningSkill, slayerSkill;
    public HashMap<String, Boolean> completedChallenges;
    public ActiveIslandChallenge challenge;

    public HashMap<UUID, IslandRole> members;
    public HashMap<ResourceNode, Integer> minedResourceNodes;
    public HashMap<ResourceNode, Double> resourceRespawnRate;
    public HashMap<String, Integer> slainMobs;
    public HashMap<String, Double> mobRespawnRate;
    public HashMap<FarmingRecipe, Integer> cropsGrown;

    public Island(Origin origin, UUID creator, Location center) {
        loadVariables();
        this.uuid = UUID.randomUUID();
        this.creationTime = System.currentTimeMillis();
        this.origin = origin;
        this.creator = creator;
        this.center = center;
        this.home = center;
        this.warp = center;
        setLevel(IslandLevel.paths.get("default"));

        this.members.put(creator, IslandRole.defaultCreator);

        farmingSkill = new ActiveIslandSkill(FarmingSkill.paths.get("default"), 1, 0.00);
        slayerSkill = new ActiveIslandSkill(SlayerSkill.paths.get("default"), 1, 0.00);
        challenge = new ActiveIslandChallenge(IslandChallenge.paths.get(IslandChallenge.paths.keySet().toArray()[0]), 0.00);
        final List<String> allowedMobs = new ArrayList<>();
        allowedMobs.add(SlayerSkill.paths.get("default").entity);
        final List<ResourceNode> allowedNodes = new ArrayList<>();
        allowedNodes.add(ResourceNode.paths.get("default"));
        this.allowedNodes.addAll(allowedNodes);
        this.allowedMobs.addAll(allowedMobs);
        this.allowedCrops.addAll(new ArrayList<>(FarmingRecipe.defaults));
        for(String s : allowedMobs) {
            slainMobs.put(s, 0);
            mobRespawnRate.put(s, 1.00);
        }
        for(FarmingRecipe f : allowedCrops) {
            final FarmingSkill skill = FarmingSkill.paths.getOrDefault(f.path, null);
            cropsGrown.put(f, skill == null || skill.required == null ? 0 : -1);
        }
        for(ResourceNode r : allowedNodes) {
            minedResourceNodes.put(r, 0);
            resourceRespawnRate.put(r, 1.00);
        }
        created();
    }
    public Island(UUID uuid, long creationTime, Origin origin, UUID creator, Location center, HashMap<UUID, IslandRole> members, ActiveIslandSkill farmingSkill, ActiveIslandSkill miningSkill, ActiveIslandSkill slayerSkill, List<ActiveIslandChallenge> challenges, List<FarmingRecipe> allowedCrops, List<ResourceNode> allowedNodes, List<String> allowedMobs, HashMap<ResourceNode, Integer> minedResourceNodes, HashMap<ResourceNode, Double> resourceRespawnRate, HashMap<String, Integer> slainMobs, HashMap<FarmingRecipe, Integer> cropsGrown, IslandLevel level) {
        loadVariables();
        this.uuid = uuid;
        this.creationTime = creationTime;
        this.origin = origin;
        this.creator = creator;
        this.center = center;
        this.home = center;
        this.warp = center;
        if(!members.containsKey(creator)) this.members.put(creator, IslandRole.defaultCreator);
        this.members.putAll(members);
        this.allowedCrops.addAll(allowedCrops);
        this.farmingSkill = farmingSkill;
        this.miningSkill = miningSkill;
        this.slayerSkill = slayerSkill;
        this.allowedNodes.addAll(allowedNodes);
        this.allowedMobs.addAll(allowedMobs);
        this.minedResourceNodes = minedResourceNodes;
        this.resourceRespawnRate = resourceRespawnRate;
        this.slainMobs = slainMobs;
        this.cropsGrown = cropsGrown;
        setLevel(level);
        created();
    }

    private void created() {
        isLoaded = true;
        file = new File(folder, uuid.toString() + ".yml");
        yml = YamlConfiguration.loadConfiguration(file);
        islands.put(uuid, this);
        for(UUID m : members.keySet()) players.put(m, this);
        backup();
    }
    private void loadVariables() {
        if(!isLoaded) {
            immuneTo = new ArrayList<>();
            members = new HashMap<>();
            banned = new ArrayList<>();
            completedChallenges = new HashMap<>();
            allowedCrops = new ArrayList<>();
            allowedNodes = new ArrayList<>();
            allowedMobs = new ArrayList<>();
            permissionBlocks = new ArrayList<>();
            activeResourceNodes = new ArrayList<>();
            unplacedResourceNodes = new ArrayList<>();
            minedResourceNodes = new HashMap<>();
            resourceRespawnRate = new HashMap<>();
            slainMobs = new HashMap<>();
            mobRespawnRate = new HashMap<>();
            cropsGrown = new HashMap<>();
            invites = new ArrayList<>();
        }
    }

    public Island(UUID uuid) {
        final File f = new File(folder, uuid.toString() + ".yml");
        if(!islands.containsKey(uuid) && f.exists()) {
            file = new File(folder, uuid.toString() + ".yml");
            yml = YamlConfiguration.loadConfiguration(file);
            islands.put(uuid, this);
            load();
            backup();
        }
    }
    public static Island get(UUID uuid) { return islands.getOrDefault(uuid, new Island(uuid)); }

    public void backup() {
        final String creationInfo = creationTime + ";" + creator;
        final String info = origin.path + ";" + level.path + ";" + tag + ";" + description;
        String members = "", crops = "", mobs = "", nodes = "";
        for(UUID m : this.members.keySet()) members = members + m + "=" + this.members.get(m).path + ";";
        yml.set("creationInfo", creationInfo);
        yml.set("center", uv.toString(center));
        yml.set("home", uv.toString(home));
        yml.set("warp", warp != null ? uv.toString(warp) : "null");
        yml.set("info", info);
        yml.set("members", members);
        for(FarmingRecipe r : allowedCrops) crops = crops + r.path + ";";
        yml.set("allowed crops", crops);
        for(String t : allowedMobs) mobs = mobs + t + ";";
        yml.set("allowed mobs", mobs);
        for(ResourceNode r : allowedNodes) nodes = nodes + r.path + ";";
        yml.set("allowed nodes", nodes);
        yml.set("challenges.active.path", challenge.type.path);
        yml.set("challenges.active.progress", challenge.progress);
        for(String s : completedChallenges.keySet()) {
            yml.set("challenges.completed." + s, completedChallenges.get(s));
        }
        final FarmingSkill f = (FarmingSkill) farmingSkill.skill;
        final SlayerSkill s = (SlayerSkill) slayerSkill.skill;
        yml.set("farming.path", f.path);
        yml.set("farming.level", farmingSkill.level);
        yml.set("farming.progress", farmingSkill.progress);
        yml.set("slayer.path", s.path);
        yml.set("slayer.level", slayerSkill.level);
        yml.set("slayer.progress", slayerSkill.progress);
        for(ResourceNode n : minedResourceNodes.keySet()) yml.set("mined resource nodes." + n.path, minedResourceNodes.get(n));
        for(String m : slainMobs.keySet()) yml.set("slain mobs." + m, slainMobs.get(m));
        for(FarmingRecipe r : cropsGrown.keySet()) yml.set("crops grown." + r.path, cropsGrown.get(r));

        for(RSInvite rsi : invites) {
            yml.set("invites." + rsi.receiver.toString(), rsi.createdTime + ";" + rsi.sender.getUUID().toString() + ";" + rsi.expireTask);
        }

        for(ActiveResourceNode arn : activeResourceNodes) {
            final String l = uv.toString(arn.location), u = arn.uuid.toString();
            yml.set("resource nodes." + u + ".location", l);
            yml.set("resource nodes." + u + ".info", arn.type.path + ";" + arn.cooldownExpiration);
        }
        yml.set("permission blocks", null);
        for(ActivePermissionBlock p : permissionBlocks) {
            final String u = p.getUUID().toString();
            String m = "";
            for(UUID uu : p.members) m = m.concat(uu.toString() + ";");
            yml.set("permission blocks." + u + ".location", uv.toString(p.getLocation()));
            yml.set("permission blocks." + u + ".members", m);
            yml.set("permission blocks." + u + ".settings", p.getType().path + ";" + p.interact + ";" + p.interactWithDoors + ";" + p.interactWithLevers + ";" + p.interactWithPressurePlates + ";" + p.interactWithHoppers + ";" + p.interactWithChests + ";" + p.interactWithEntities + ";" + p.damageEntities + ";" + p.pickupExp + ";" + p.pickupItems + ";" + p.dropItems + ";" + p.placeBlocks + ";" + p.breakBlocks + ";" + p.harvestResourceNodes + ";" + p.publicRegion + ";" + p.safePVP);
        }
        save();
    }
    private void save() {
        try {
            yml.save(file);
            yml = YamlConfiguration.loadConfiguration(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void load() {
        if(!isLoaded) {
            loadVariables();
            final BukkitScheduler sch = uv.scheduler;
            isLoaded = true;
            members.clear();
            completedChallenges.clear();
            allowedMobs.clear();
            allowedCrops.clear();
            minedResourceNodes.clear();
            slainMobs.clear();
            cropsGrown.clear();
            for(ActiveResourceNode a : activeResourceNodes) sch.cancelTask(a.respawnTask);
            activeResourceNodes.clear();
            immuneTo.clear();
            invites.clear();
            permissionBlocks.clear();

            final String[] creationInfo = yml.getString("creationInfo").split(";"), info = yml.getString("info").split(";"), m = yml.getString("members").split(";");
            final String[] crops = yml.getString("allowed crops").split(";"), mobs = yml.getString("allowed mobs").split(";"), nodes = yml.getString("allowed nodes").split(";");
            this.creationTime = Long.parseLong(creationInfo[0]);
            this.creator = UUID.fromString(creationInfo[1]);
            this.center = uv.toLocation(yml.getString("center"));
            this.home = uv.toLocation(yml.getString("home"));
            final String w = yml.getString("warp");
            this.warp = w != null && !w.equals("null") ? uv.toLocation(w) : null;
            this.origin = Origin.paths.get(info[0]);
            setLevel(IslandLevel.paths.get(info[1]));
            this.tag = info[2];
            this.description = info[3];
            for(String s : m) {
                final UUID u = UUID.fromString(s.split("=")[0]);
                members.put(u, IslandRole.paths.getOrDefault(s.split("=")[1], IslandRole.defaultMember));
                players.put(u, this);
            }
            for(String s : crops) allowedCrops.add(FarmingRecipe.paths.get(s));
            for(String s : mobs) allowedMobs.add(s);
            for(String s : nodes) allowedNodes.add(ResourceNode.paths.get(s));

            final ConfigurationSection minedresourcenodes = yml.getConfigurationSection("mined resource nodes"), slainmobs = yml.getConfigurationSection("slain mobs"), cropsg = yml.getConfigurationSection("crops grown");
            if(minedresourcenodes != null) {
                for(String s : minedresourcenodes.getKeys(false)) {
                    minedResourceNodes.put(ResourceNode.paths.get(s), yml.getInt("mined resource nodes." + s));
                }
            }
            if(slainmobs != null) {
                for(String s : slainmobs.getKeys(false)) {
                    slainMobs.put(s, yml.getInt("slain mobs." + s));
                }
            }
            if(cropsg != null) {
                for(String s : cropsg.getKeys(false)) {
                    cropsGrown.put(FarmingRecipe.paths.get(s), yml.getInt("crops grown." + s));
                }
            }

            final ConfigurationSection inv = yml.getConfigurationSection("invites"), c = yml.getConfigurationSection("challenges.completed"), rn = yml.getConfigurationSection("resource nodes"), pb = yml.getConfigurationSection("permission blocks");
            if(inv != null) {
                for(String s : inv.getKeys(false)) {
                    final String[] in = yml.getString("invites." + s).split(";");
                    invites.add(new RSInvite(Long.parseLong(in[0]), RSPlayer.get(UUID.fromString(in[1])), UUID.fromString(s), InviteType.ISLAND, Integer.parseInt(in[2])));
                }
            }

            challenge = new ActiveIslandChallenge(IslandChallenge.paths.get(yml.getString("challenges.active.path")), yml.getDouble("challenges.active.progress"));
            if(c != null) {
                for(String s : c.getKeys(false)) {
                    completedChallenges.put(s, yml.getBoolean("challenges.completed." + s));
                }
            }
            farmingSkill = new ActiveIslandSkill(FarmingSkill.paths.get(yml.getString("farming.path")), yml.getInt("farming.level"), yml.getDouble("farming.progress"));
            slayerSkill = new ActiveIslandSkill(SlayerSkill.paths.get(yml.getString("slayer.path")), yml.getInt("slayer.level"), yml.getDouble("slayer.progress"));

            if(rn != null) {
                for(String s : rn.getKeys(false)) {
                    final Location l = uv.toLocation(yml.getString("resource nodes." + s + ".location"));
                    final String[] i = yml.getString("resource nodes." + s + ".info").split(";");
                    new ActiveResourceNode(UUID.fromString(s), ResourceNode.paths.get(i[0]), l, Long.parseLong(i[1]), false);
                }
            }
            if(pb != null) {
                for(String s : pb.getKeys(false)) {
                    final String p = "permission blocks." + s + ".";
                    final Location loc = uv.toLocation(yml.getString(p + "location"));
                    final String[] mem = yml.getString(p + "members").split(";"), settings = yml.getString(p + "settings").split(";");
                    final List<UUID> M = new ArrayList<>();
                    if(mem.length > 0) {
                        for(String Q : mem) {
                            if(!Q.isEmpty()) {
                                M.add(UUID.fromString(Q));
                            }
                        }
                    }
                    final Boolean[] x = new Boolean[settings.length-1];
                    for(int i = 1; i < settings.length; i++) {
                        x[i-1] = Boolean.parseBoolean(settings[i]);
                    }
                    final ActivePermissionBlock apb = new ActivePermissionBlock(UUID.fromString(s), loc, PermissionBlock.paths.getOrDefault(settings[0], null), M, x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7], x[8], x[9], x[10], x[11], x[12], x[13], x[14], x[15]);
                    permissionBlocks.add(apb);
                }
            }
        }
    }

    public boolean isLoaded() { return isLoaded; }
    public boolean isOpenToPublic() { return openToPublic; }
    public void setOpenToPublic(boolean openToPublic) { this.openToPublic = openToPublic; }
    public UUID getUUID() { return uuid; }
    public UUID getCreator() { return creator; }
    public YamlConfiguration getYaml() { return yml; }
    public long getCreationTime() { return creationTime; }
    public Origin getOrigin() { return origin; }
    public void setOrigin(Origin origin) {
        this.origin = origin;
        setIslandPerks();
    }
    public Location getCenter() { return center; }
    public Location getHome() { return home; }
    public void setHome(Location home) { this.home = home; }
    public Location getWarp() { return warp; }
    public void setWarp(Location warp) { this.warp = warp; }

    public void placeUnplacedResourceNodes() {
        for(ActiveResourceNode a : unplacedResourceNodes) {
            a.place();
        }
        unplacedResourceNodes.clear();
    }

    public List<Player> getOnlineMembers() {
        final List<Player> o = new ArrayList<>();
        for(UUID u : members.keySet()) {
            final OfflinePlayer p = Bukkit.getOfflinePlayer(u);
            if(p != null && p.isOnline()) o.add(p.getPlayer());
        }
        return o;
    }
    public void delete() {
        islands.remove(uuid);
        players.remove(creator);
        for(ActiveResourceNode a : activeResourceNodes) {
            a.delete();
        }
        for(UUID m : members.keySet()) {
            remove(m);
        }
        for(RSInvite i : invites) {
            i.delete();
        }
        file.delete();
    }
    public void setLevel(IslandLevel level) {
        this.level = level;
        for(String s : level.rewards) {
            final String r = s.toLowerCase();
            if(r.startsWith("islandradius=")) {
                radius = Integer.parseInt(r.split("=")[1]);
                boundary = new PolyBoundary(center, radius);
            } else if(r.startsWith("spawnerlimit=")) {
                spawnerLimit = Integer.parseInt(r.split("=")[1]);
            } else if(r.startsWith("maxmembers=")) {
                maxMembers = Integer.parseInt(r.split("=")[1]);
            }
        }
        setIslandPerks();
    }
    public void setIslandPerks() {
        for(String S : origin.perks) {
            final String s = S.toLowerCase();
            if(s.startsWith("xpgainmultiplier=")) {
                XPGainMultiplier = Double.parseDouble(s.split("=")[1]);
            } else if(s.startsWith("outgoingpvedamagemultiplier=")) {
                outgoingPvEDamageMultiplier = Double.parseDouble(s.split("=")[1]);
            } else if(s.startsWith("incomingpvpdamagemultiplier=")) {
                incomingPvPDamageMultiplier = Double.parseDouble(s.split("=")[1]);
            } else if(s.startsWith("outgoingpvpdamagemultiplier=")) {
                outgoingPvPDamageMultiplier = Double.parseDouble(s.split("=")[1]);
            } else if(s.startsWith("sellpricemultiplier=")) {
                sellPriceMultiplier = Double.parseDouble(s.split("=")[1]);
            } else if(s.startsWith("immunetope=")) {
                immuneTo.add(PotionEffectType.getByName(s.split("=")[1].toUpperCase()));
            }
        }
    }
    public double getResourceNodeValue() {
        double v = 0.00;
        for(ActiveResourceNode a : activeResourceNodes) {
            v += a.type.value;
        }
        return v;
    }
    public void join(Player player) {
        final UUID u = player.getUniqueId();
        members.put(u, IslandRole.defaultMember);
    }
    private void remove(UUID uuid) {
        members.remove(uuid);
    }
    public void ban(OfflinePlayer player) {
        final UUID uuid = player.getUniqueId();
        banned.add(uuid);
        remove(uuid);
    }
    public void unban(OfflinePlayer player) {
        banned.remove(player.getUniqueId());
    }
    public int getMinedResourceNodes(ResourceNode type) {
        return minedResourceNodes.getOrDefault(type, 0);
    }
    public ActiveResourceNode valueof(Location l) {
        for(ActiveResourceNode a : activeResourceNodes) {
            if(a.location.equals(l)) {
                return a;
            }
        }
        return null;
    }
    public ActivePermissionBlock valueOF(Location l) {
        if(l != null) {
            for(ActivePermissionBlock a : permissionBlocks) {
                if(a.getLocation().equals(l)) {
                    return a;
                }
            }
        }
        return null;
    }
    public List<ActivePermissionBlock> getNearbyPermissionBlocks(Location l) {
        final List<ActivePermissionBlock> b = new ArrayList<>();
        for(ActivePermissionBlock a : permissionBlocks) {
            if(a.getBoundary().contains(l)) {
                b.add(a);
            }
        }
        return b;
    }

    public static Island valueOf(Location l) {
        if(l != null && islands != null && !islands.isEmpty()) {
            final Location a = l.clone();
            a.setY(100);
            for(Island is : islands.values()) {
                if(is != null && is.boundary != null && is.boundary.contains(a)) {
                    return is;
                }
            }
        }
        return null;
    }
}
