package me.randomhashtags.randomsky.utils.classes.island;

public class ActiveIslandSkill {
    public Object skill;
    public int level;
    public double progress;
    public ActiveIslandSkill(Object skill, int level, double progress) {
        this.skill = skill;
        this.level = level;
        this.progress = progress;
    }
}
