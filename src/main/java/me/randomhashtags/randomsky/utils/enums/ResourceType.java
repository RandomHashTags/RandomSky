package me.randomhashtags.randomsky.utils.enums;

public enum ResourceType {
    RESOURCE, RESOURCE_ITEM, COSMETIC, SCRAP, FRAGMENT,
}
