package me.randomhashtags.randomsky.utils;

import me.randomhashtags.randomsky.RandomSky;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class Updater {

    private static Updater instance;
    public static final Updater getUpdater() {
        if(instance == null) instance = new Updater();
        return instance;
    }
    public boolean updateIsAvailable = true;

    public void checkForUpdate() {
        try {
            final URL checkURL = new URL("https://api.spigotmc.org/legacy/update.php?resource=%%__RESOURCE__%%");
            final URLConnection con = checkURL.openConnection();
            final String v = RandomSky.getPlugin.getDescription().getVersion(), newVersion = new BufferedReader(new InputStreamReader(con.getInputStream())).readLine();
            final boolean canUpdate = !v.equals(newVersion);
            updateIsAvailable = canUpdate;
            if(canUpdate) Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomSky] &eUpdate available! &aYour version: &f" + v + "&a. Latest version: &f" + newVersion));
        } catch(Exception e) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomSky] &cCould not check for updates due to being unable to connect to SpigotMC!"));
        }
    }
}
