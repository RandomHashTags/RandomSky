package me.randomhashtags.randomsky.utils.supported;

import me.clip.placeholderapi.external.EZPlaceholderHook;
import me.randomhashtags.randomsky.RandomSky;
import me.randomhashtags.randomsky.utils.RSPlayer;
import me.randomhashtags.randomsky.utils.classes.PlayerRank;
import me.randomhashtags.randomsky.utils.classes.alliances.Alliance;
import org.bukkit.entity.Player;

import java.util.UUID;

public class Placeholders extends EZPlaceholderHook {

    public Placeholders(RandomSky rs) {
        super(rs, "randomsky");
    }

    @Override
    public String onPlaceholderRequest(Player player, String identifier) {
        final UUID u = player != null ? player.getUniqueId() : null;
        final RSPlayer pdata = u != null ? RSPlayer.get(u) : null;
        if(pdata == null) {
            return "";
        } else if(identifier.equals("ah_listings")) {
            return Integer.toString(pdata.auctions.size());

        } else if(identifier.startsWith("alliance_")) {
            String r = "";
            final Alliance a = Alliance.players.getOrDefault(u, null);
            if(a != null) {
                if(identifier.endsWith("_tag")) r = a.getTag();
                else if(identifier.endsWith("_role")) r = a.getMember(player).role.chatTag;
            }
            return r;

        } else if(identifier.equals("coinflip_wins")) {
            return Integer.toString(pdata.coinflipWins);
        } else if(identifier.equals("coinflip_won$")) {
            return Double.toString(pdata.coinflipWonCash);
        } else if(identifier.equals("coinflip_losses")) {
            return Integer.toString(pdata.coinflipLosses);
        } else if(identifier.equals("coinflip_lost$")) {
            return Double.toString(pdata.coinflipLostCash);
        } else if(identifier.equals("coinflip_notifications")) {
            return Boolean.toString(pdata.coinflipNotifications);

        } else if(identifier.equals("jackpot_countdown")) {
            return Boolean.toString(pdata.jackpotCountdown);
        } else if(identifier.equals("jackpot_wins")) {
            return Integer.toString(pdata.jackpotWins);
        } else if(identifier.equals("jackpot_won$")) {
            return Long.toString(pdata.jackpotWonCash);
        } else if(identifier.equals("jackpot_tickets")) {
            return Integer.toString(pdata.jackpotTickets);

        } else if(identifier.equals("rank")) {
            final PlayerRank rank = pdata.getRank();
            return rank != null ? rank.appearance : "";
        }
        return null;
    }
}