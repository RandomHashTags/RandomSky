package me.randomhashtags.randomsky.utils;

import me.randomhashtags.randomsky.RandomSkyAPI;
import me.randomhashtags.randomsky.utils.classes.alliances.Alliance;
import me.randomhashtags.randomsky.utils.classes.island.Island;
import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.io.File;
import java.util.UUID;

public class RSEvents extends RandomSkyAPI implements Listener  {

    private static RSEvents instance;
    public static final RSEvents getRSEvents() {
        if(instance == null) instance = new RSEvents();
        return instance;
    }

    private int task;
    private boolean isEnabled = false, oldMechanics;

    public void enable() {
        if(isEnabled) return;
        isEnabled = true;
        pluginmanager.registerEvents(this, randomsky);

        loadBackup();
        final FileConfiguration f = randomsky.getConfig();
        final int backups = f.getInt("backups");
        oldMechanics = f.getBoolean("old mechanics");
        task = scheduler.scheduleSyncRepeatingTask(randomsky, () -> backup(true), backups, 20*60*backups);
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        scheduler.cancelTask(task);
        HandlerList.unregisterAll(this);
    }

    public void backup(boolean async) {
        if(async) scheduler.runTaskAsynchronously(randomsky, () -> dobackup());
        else dobackup();
    }
    private void dobackup() {
        for(RSPlayer pdata : RSPlayer.players.values()) {
            pdata.backup();
        }
        for(Island island : Island.islands.values()) {
            island.backup();
        }
        for(Alliance a : Alliance.alliances.values()) {
            a.backup();
        }
    }
    public void loadBackup() {
        scheduler.runTaskAsynchronously(randomsky, () -> {
            final long started = System.currentTimeMillis();
            int loaded = 0, a = 0;
            RSPlayer.players.clear();
            Island.players.clear();
            Island.islands.clear();
            Alliance.alliances.clear();
            Alliance.players.clear();
            Alliance.tags.clear();
            for(Player player : Bukkit.getOnlinePlayers()) {
                RSPlayer.get(player.getUniqueId()).load();
                loaded++;
            }
            final long s = System.currentTimeMillis();
            sendConsoleMessage("&6[RandomSky] &aLoaded " + loaded + " online player data &e(took " + (s-started) + "ms) [async]");
            final File[] really = new File(rsd + separator + "_Data" + separator + "alliances").listFiles();
            if(really != null) {
                for(File f : really) {
                    Alliance.get(UUID.fromString(f.getName().split("\\.yml")[0])).load();
                    a++;
                }
            }
            sendConsoleMessage("&6[RandomSky] &aLoaded " + a + " Alliance data &e(took " + (System.currentTimeMillis()-s) + "ms) [async] ");
        });
        for(Island is : Island.islands.values()) {
            is.placeUnplacedResourceNodes();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void playerJoinEvent(PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        RSPlayer.get(player.getUniqueId()).load();
        final AttributeInstance a = player.getAttribute(Attribute.GENERIC_ATTACK_SPEED);
        a.setBaseValue(oldMechanics ? a.getDefaultValue() : 27.00);
    }
    @EventHandler
    private void playerQuitEvent(PlayerQuitEvent event) {
        RSPlayer.get(event.getPlayer().getUniqueId()).unload();
    }
}
