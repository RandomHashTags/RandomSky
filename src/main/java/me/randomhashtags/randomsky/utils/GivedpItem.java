package me.randomhashtags.randomsky.utils;

import me.randomhashtags.randomsky.RandomSkyAPI;
import org.bukkit.command.CommandExecutor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.HashMap;

public class GivedpItem extends RandomSkyAPI implements Listener, CommandExecutor {

    private static GivedpItem instance;
    public static final GivedpItem getGivedpItem() {
        if(instance == null) instance = new GivedpItem();
        return instance;
    }

    private boolean isEnabled = false;
    public YamlConfiguration itemsConfig;

    public HashMap<String, ItemStack> items;
    private int flightOrbMax;

    public void load() {
        if(isEnabled) return;
        save(null, "items.yml");
        itemsConfig = YamlConfiguration.loadConfiguration(new File(randomsky.getDataFolder(), "items.yml"));
        pluginmanager.registerEvents(this, randomsky);
        for(String s : itemsConfig.getConfigurationSection("custom items").getKeys(false)) {
            customitems.put(s, d(itemsConfig, "custom items." + s));
        }
        sendConsoleMessage("&6[RandomSky] &aLoaded " + customitems.size() + " Custom Items");
        items = new HashMap<>();
        items.put("banknote", d(itemsConfig, "banknote"));
        items.put("flightorb", d(itemsConfig, "flight orb"));
        flightOrbMax = itemsConfig.getInt("flight orb.max");
        items.put("itemlorecrystal", d(itemsConfig, "item lore crystal"));
        items.put("itemnametag", d(itemsConfig, "item name tag"));
        items.put("spacedrink", d(itemsConfig, "space drink"));
        items.put("totemofundying", d(itemsConfig, "totem of undying"));
        items.put("xpbottle", d(itemsConfig, "xpbottle"));
        isEnabled = true;
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        itemsConfig = null;
        items = null;
        HandlerList.unregisterAll(this);
    }

    public ItemStack valueOf(String input) {
        final String Q = input.split(";")[0];
        input = input.toLowerCase();
        if(input.startsWith("banknote:")) {
            final String[] a = Q.split(":");
            return getBanknote(Integer.parseInt(a[1]), a.length == 3 ? a[2] : null);
        } else if(input.startsWith("flightorb")) {
            final int uses = input.contains(":") ? Integer.parseInt(Q.split(":")[1]) : flightOrbMax;
            return getFlightOrb(uses);
        } else if(input.startsWith("itemlorecrystal")) {
            return items.get("itemlorecrystal").clone();
        } else if(input.startsWith("itemnametag")) {
            return items.get("itemnametag").clone();
        } else if(input.startsWith("spacedrink")) {
            return items.get("spacedrink").clone();
        } else if(input.startsWith("totemofundying")) {
            return items.get("totemofundying").clone();
        } else if(input.startsWith("xpbottle:")) {
            final String[] a = Q.split(":");
            return getXPBottle(Integer.parseInt(a[1]), a.length == 3 ? a[2] : null);
        }
        return null;
    }

    public ItemStack getBanknote(double value, String signer) {
        item = items.get("banknote").clone(); itemMeta = item.getItemMeta(); lore.clear();
        for(String s : itemMeta.getLore()) {
            if(s.contains("{SIGNER}")) s = signer != null ? s.replace("{SIGNER}", signer) : null;
            if(s != null) lore.add(s.replace("{VALUE}", formatDouble(value)));
        }
        itemMeta.setLore(lore); lore.clear();
        item.setItemMeta(itemMeta);
        return item;
    }
    public ItemStack getFlightOrb(int charges) {
        item = items.get("flightorb").clone(); itemMeta = item.getItemMeta(); lore.clear();
        for(String s : itemMeta.getLore()) {
            lore.add(s.replace("{CHARGES}", Integer.toString(charges)).replace("{MAX_CHARGES}", Integer.toString(flightOrbMax)));
        }
        itemMeta.setLore(lore); lore.clear();
        item.setItemMeta(itemMeta);
        return item;
    }
    public ItemStack getXPBottle(int value, String enchanter) {
        item = items.get("xpbottle").clone(); itemMeta = item.getItemMeta(); lore.clear();
        for(String s : itemMeta.getLore()) {
            if(s.contains("{ENCHANTER}")) s = enchanter != null ? s.replace("{ENCHANTER}", enchanter) : null;
            if(s != null) lore.add(s.replace("{VALUE}", formatInt(value)));
        }
        itemMeta.setLore(lore); lore.clear();
        item.setItemMeta(itemMeta);
        return item;
    }
}
