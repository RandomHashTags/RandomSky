package me.randomhashtags.randomsky.utils.universal;

import org.bukkit.Bukkit;
import org.bukkit.Sound;

import java.util.HashMap;

public enum USound {

    // <usound enum>(1.8.8, 1.9.4, 1.10.2, 1.11.2, 1.12.2, 1.13.2, 1.14.0)
    AMBIENCE_CAVE("AMBIENCE_CAVE", "AMBIENT_CAVE"),
    AMBIENT_UNDERWATER_ENTER(null, null, null, null, null, "AMBIENT_UNDERWATER_ENTER"),
    AMBIENT_UNDERWATER_EXIT(null, null, null, null, null, "AMBIENT_UNDERWATER_EXIT"),
    AMBIENT_UNDERWATER_LOOP(null, null, null, null, null, "AMBIENT_UNDERWATER_LOOP"),
    AMBIENT_UNDERWATER_LOOP_ADDITIONS(null, null, null, null, null, "AMBIENT_UNDERWATER_LOOP_ADDITIONS"),
    AMBIENT_UNDERWATER_LOOP_ADDITIONS_RARE(null, null, null, null, null, "AMBIENT_UNDERWATER_LOOP_ADDITIONS_RARE"),
    AMBIENT_UNDERWATER_LOOP_ADDITIONS_ULTRA_RARE(null, null, null, null, null, "AMBIENT_UNDERWATER_LOOP_ADDITIONS_ULTRA_RARE"),
    // Animals
    BAT_TAKEOFF("BAT_TAKEOFF", "ENTITY_BAT_TAKEOFF"),
    // Blocks
    ANVIL_BREAK("ANVIL_BREAK", "BLOCK_ANVIL_BREAK"),
    ANVIL_DESTROY(null, "BLOCK_ANVIL_DESTROY"),
    ANVIL_FALL("ANVIL_FALL", "BLOCK_ANVIL_FALL"),
    // Weather
    WEATHER_RAIN("AMBIENCE_RAIN", "WEATHER_RAIN"),
    THUNDER("AMBIENCE_THUNDER", "ENTITY_LIGHTNING_THUNDER")
    ;

    private static final HashMap<String, USound> memory = new HashMap<>();
    private static final String v = Bukkit.getVersion();
    private final String[] names;
    private final String soundVersionName;

    USound(String...names) {
        this.names = names;
        this.soundVersionName = getVersionName();
    }
    private String getVersionName() {
        int i = v.contains("1.8") ? 0 : v.contains("1.9") ? 1 : v.contains("1.10") ? 2 : v.contains("1.11") ? 3 : v.contains("1.12") ? 4 : v.contains("1.13") ? 5 : 6;
        final int l = names.length;
        String n = i < l ? names[i] : names[l-1];
        if(n == null) {

        }
        return n;
    }

    public Sound getSound() {
        return Sound.valueOf(soundVersionName);
    }

    public static USound match(String name) {
        if(name != null) {
            name = name.toUpperCase();
            if(memory.containsKey(name)) return memory.get(name);
            try {
                return USound.valueOf(name);
            } catch (NullPointerException e) {
                for(USound u : USound.values()) {
                    for(String s : u.names) {
                        if(s.equals(name)) {
                            memory.put(name, u);
                            return u;
                        }
                    }
                }
            }
        }
        return null;
    }
}
