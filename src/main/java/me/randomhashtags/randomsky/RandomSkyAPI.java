package me.randomhashtags.randomsky;

import me.randomhashtags.randomsky.api.*;
import me.randomhashtags.randomsky.api.unfinished.CollectionChests;
import me.randomhashtags.randomsky.utils.GivedpItem;
import me.randomhashtags.randomsky.utils.RSEvents;
import me.randomhashtags.randomsky.utils.RSPlayer;
import me.randomhashtags.randomsky.utils.classes.*;
import me.randomhashtags.randomsky.utils.classes.adventures.Adventure;
import me.randomhashtags.randomsky.utils.classes.chat.ChatChannel;
import me.randomhashtags.randomsky.utils.classes.chat.ChatChannels;
import me.randomhashtags.randomsky.utils.classes.island.FarmingRecipe;
import me.randomhashtags.randomsky.utils.classes.island.ItemRecipe;
import me.randomhashtags.randomsky.utils.classes.island.PermissionBlock;
import me.randomhashtags.randomsky.utils.classes.kits.SkyKit;
import me.randomhashtags.randomsky.utils.classes.resources.Resource;
import me.randomhashtags.randomsky.utils.classes.resources.ResourceNode;
import me.randomhashtags.randomsky.utils.enums.ResourceType;
import me.randomhashtags.randomsky.utils.supported.VaultAPI;
import me.randomhashtags.randomsky.utils.universal.UMaterial;
import me.randomhashtags.randomsky.utils.universal.UVersion;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.*;
import org.bukkit.command.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.MaterialData;

import java.io.File;
import java.util.*;

public class RandomSkyAPI extends UVersion implements CommandExecutor, Listener {

    private static RandomSkyAPI instance;
    public static final RandomSkyAPI getAPI() {
        if(instance == null) instance = new RandomSkyAPI();
        return instance;
    }

    public static Economy eco;
    public static HashMap<String, ItemStack> customitems;

    private boolean isEnabled = false, epicSpawners;
    private FileConfiguration config;
    private static ConsoleCommandSender console;

    public static String separator;
    public static File rsd;
    private static File otherF;
    public static YamlConfiguration otherdata;

    private HashMap<Integer, ChatChannel> channels;
    private HashMap<String, String> channelz;
    private ItemStack chatCurrent, chatActive, chatDisabled, chatBackground, air;
    private String chatcurrent, chatactive, chatdisabled;
    private Inventory chatInv;

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        final Player player = sender instanceof Player ? (Player) sender : null;
        final String c = cmd.getName();
        final int l = args.length;
        if(c.equals("randomsky")) {
            if(l == 0 && (player != null && player.getName().equals("RandomHashTags") || hasPermission(sender, "RandomSky.randomsky", true))) {
                for(String string : Arrays.asList(" ",
                        "&6&m&l---------------------------------------------",
                        "&7- Author: &6RandomHashTags",
                        "&7- RandomSky Version: &b" + randomsky.getDescription().getVersion(),
                        "&7- Server Version: &f" + version,
                        "&7- WorldEdit Version: &e" + pluginmanager.getPlugin("WorldEdit").getDescription().getVersion(),
                        "&7- FastAsyncWorldEdit Version: &e" + pluginmanager.getPlugin("FastAsyncWorldEdit").getDescription().getVersion(),
                        "&7- Multiverse-Core Version: &e" + pluginmanager.getPlugin("Multiverse-Core").getDescription().getVersion(),
                        "&7- Info: &f%%__USER__%%, &f%%__NONCE__%%",
                        "&7- Purchaser: &a&nhttps://www.spigotmc.org/members/%%__USER__%%/",
                        "&6&m&l---------------------------------------------",
                        " "))
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
            } else if(l == 1) {
                final String a = args[0];
                if(a.equals("reload") && hasPermission(sender, "RandomSky.randomsky.reload", true)) {
                    RandomSky.getPlugin.reload();
                } else if(a.equals("backup") && hasPermission(sender, "RandomSky.randomsky.backup", true)) {
                    RSEvents.getRSEvents().backup(true);
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomSky] &aData backup complete!"));
                }
            }
        } else if(c.equals("chat") && player != null) {
            viewChatChannels(player);
        } else if(c.equals("tinkerer") && player != null) {

        } else if(c.equals("toggle") && player != null) {
            viewToggles(player);
        }
        return true;
    }


    public void load() {
        final long started = System.currentTimeMillis();
        if(isEnabled) return;
        epicSpawners = pluginmanager.isPluginEnabled("EpicSpawners");
        eco = VaultAPI.getVaultAPI().economy;
        config = randomsky.getConfig();
        separator = File.separator;
        rsd = randomsky.getDataFolder();
        otherF = new File(randomsky.getDataFolder() + separator + "_Data" + separator, "other.yml");
        otherdata = YamlConfiguration.loadConfiguration(otherF);
        pluginmanager.registerEvents(this, randomsky);
        isEnabled = true;

        console = Bukkit.getConsoleSender();
        customitems = new HashMap<>();
        channels = new HashMap<>();
        channelz = new HashMap<>();
        air = new ItemStack(Material.AIR);

        final String T = config.getString("chat.gui.type").toUpperCase(), TT = ChatColor.translateAlternateColorCodes('&', config.getString("chat.gui.title"));
        final InventoryType t = !T.equals("") ? InventoryType.valueOf(T) : null;
        if(t != null) chatInv = Bukkit.createInventory(null, t, TT);
        else chatInv = Bukkit.createInventory(null, config.getInt("chat.gui.size"), TT);
        chatCurrent = d(config, "chat.gui.settings.current");
        chatcurrent = ChatColor.translateAlternateColorCodes('&', config.getString("chat.gui.settings.current.string"));
        chatActive = d(config, "chat.gui.settings.active");
        chatactive = ChatColor.translateAlternateColorCodes('&', config.getString("chat.gui.settings.active.string"));
        chatDisabled = d(config, "chat.gui.settings.disabled");
        chatdisabled = ChatColor.translateAlternateColorCodes('&', config.getString("chat.gui.settings.disabled.string"));
        chatBackground = d(config, "chat.gui.background");
        lore.clear();
        for(String s : config.getStringList("chat.settings.format")) lore.add(ChatColor.translateAlternateColorCodes('&', s));
        for(int i = 1; i <= 6; i++) {
            final String chat = i == 1 ? "GLOBAL" : i == 2 ? "ISLAND" : i == 3 ? "ALLIANCE" : i == 4 ? "ALLY" : i == 5 ? "TRUCE" : "LOCAL";
            final int slot = config.getInt("chat.gui." + chat + ".slot");
            channels.put(slot, ChatChannel.valueOf(chat));
            channelz.put(chat, ChatColor.translateAlternateColorCodes('&', config.getString("chat.messages.channels." + chat.toLowerCase())));
            item = UMaterial.ORANGE_STAINED_GLASS_PANE.getItemStack();
            itemMeta = item.getItemMeta(); lore.clear();
            itemMeta.setDisplayName(config.getString("chat.gui." + chat + ".name"));
            itemMeta.setLore(lore);
            item.setItemMeta(itemMeta);
            chatInv.setItem(slot, item);
        }
        lore.clear();
        for(int i = 0; i < chatInv.getSize(); i++) if(chatInv.getItem(i) == null) chatInv.setItem(i, chatBackground);

        sendConsoleMessage("&6[RandomSky] &aLoaded API &e(took " + (System.currentTimeMillis()-started) + "ms)");
    }
    public void saveOtherData() {
        try {
            otherdata.save(otherF);
            otherF = new File(randomsky.getDataFolder() + separator + "_Data" + separator, "other.yml");
            otherdata = YamlConfiguration.loadConfiguration(otherF);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        eco = null;
        customitems = null;
        config = null;
        console = null;
        separator = null;
        rsd = null;
        otherF = null;
        otherdata = null;
        channels = null;
        channelz = null;
        chatCurrent = null;
        chatActive = null;
        chatDisabled = null;
        chatBackground = null;
        chatcurrent = null;
        chatactive = null;
        chatdisabled = null;
        chatInv = null;
        HandlerList.unregisterAll(this);
    }

    public void viewToggles(Player player) {

    }
    public void toggleToggle(Player player, int slot) {

    }
    public void viewChatChannels(Player player) {
        if(hasPermission(player, "RandomSky.chat.view", true)) {
            final RSPlayer pdata = RSPlayer.get(player.getUniqueId());
            final ChatChannels chat = pdata.getChat();
            player.openInventory(Bukkit.createInventory(player, chatInv.getType(), chatInv.getTitle()));
            final Inventory top = player.getOpenInventory().getTopInventory();
            top.setContents(chatInv.getContents());
            for(int i : channels.keySet()) {
                top.setItem(i, getStatus(chat, channels.get(i)));
            }
            player.updateInventory();
        }
    }
    private ItemStack getStatus(ChatChannels cc, ChatChannel target) {
        final ChatChannel active = cc.current;
        lore.clear();
        final String status, T = target.name();
        if(target.equals(active)) {
            item = chatCurrent.clone();
            status = chatcurrent;
        } else if(target.equals(ChatChannel.GLOBAL) && cc.global || target.equals(ChatChannel.ISLAND) && cc.island || target.equals(ChatChannel.ALLIANCE) && cc.alliance || target.equals(ChatChannel.ALLY) && cc.ally || target.equals(ChatChannel.TRUCE) && cc.truce || target.equals(ChatChannel.LOCAL) && cc.local) {
            item = chatActive.clone();
            status = chatactive;
        } else {
            item = chatDisabled.clone();
            status = chatdisabled;
        }
        itemMeta = item.getItemMeta(); lore.clear();
        itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{NAME}", ChatColor.translateAlternateColorCodes('&', config.getString("chat.gui." + T + ".name"))));
        for(String s : colorizeListString(config.getStringList("chat.gui.settings.format"))) {
            if(s.equals("{DESC}")) {
                for(String S : colorizeListString(config.getStringList("chat.gui." + T + ".lore")))
                    lore.add(ChatColor.translateAlternateColorCodes('&', S));
            } else if(s.equals("{STATUS}")) {
                lore.add(ChatColor.translateAlternateColorCodes('&', status));
            } else {
                lore.add(s.replace("{STATUS}", status));
            }
        }
        itemMeta.setLore(lore); lore.clear();
        item.setItemMeta(itemMeta);
        return item;
    }
    public void toggleChat(Player player, Inventory top, int slot, ClickType ct, ChatChannel cc) {
        final boolean isRight = ct.isRightClick();
        final RSPlayer pdata = RSPlayer.get(player.getUniqueId());
        final ChatChannels chat = pdata.getChat();
        final List<ChatChannel> active = chat.active;
        final ChatChannel current = chat.current;
        final HashMap<String, String> replacements = new HashMap<>();
        replacements.put("{CC}", channelz.get(cc.name()));
        final List<String> msg;
        if(isRight && current.equals(cc)) {
            msg = config.getStringList("chat.messages.cannot toggle while active");
        } else if(isRight && active.contains(cc)) {
            msg = config.getStringList("chat.messages.disable");
            active.remove(cc);
            togglechat(chat, cc);
        } else if(isRight && !active.contains(cc)) {
            msg = config.getStringList("chat.messages.enable");
            active.add(cc);
            togglechat(chat, cc);
        } else { // is left
            msg = config.getStringList("chat.messages.new active");
            final ChatChannel prev = chat.current;
            final int s = getChannelSlot(prev);
            chat.current = cc;
            top.setItem(s, getStatus(chat, prev));
        }
        sendStringListMessage(player, msg, replacements);
        top.setItem(slot, getStatus(chat, cc));
        player.updateInventory();
    }
    private void togglechat(ChatChannels chat, ChatChannel cc) {
        if(cc.equals(ChatChannel.LOCAL)) {
            chat.local = !chat.local;
        } else if(cc.equals(ChatChannel.TRUCE)) {
            chat.truce = !chat.truce;
        } else if(cc.equals(ChatChannel.ALLY)) {
            chat.ally = !chat.ally;
        } else if(cc.equals(ChatChannel.ALLIANCE)) {
            chat.alliance = !chat.alliance;
        } else if(cc.equals(ChatChannel.ISLAND)) {
            chat.island = !chat.island;
        } else if(cc.equals(ChatChannel.GLOBAL)) {
            chat.global = !chat.global;
        }
    }
    private int getChannelSlot(ChatChannel cc) {
        return indexOf(channels.values(), cc);
    }

    public void sendConsoleMessage(String msg) {
        console.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
    }
    public boolean hasPermission(CommandSender sender, String permission, boolean sendNoPermMessage) {
        if(!(sender instanceof Player) || sender.hasPermission(permission)) return true;
        else if(sendNoPermMessage) {
            for(String s : Arrays.asList("")) {
            }
        }
        return false;
    }
    public void spawnParticle(RSPlayer pdata, World w, Location l, ItemStack i) {
        if(pdata.breakParticles) {
            w.spawnParticle(Particle.BLOCK_CRACK, l.add(0.5, 0.5, 0.5), 100, new MaterialData(i.getType(), i.getData().getData()));
        }
    }
    public void sendStringListMessage(CommandSender sender, List<String> message, HashMap<String, String> replacements) {
        if(sender != null && message != null && !message.isEmpty() && !message.get(0).equals("")) {
            for(String s : message) {
                if(replacements != null) for(String r : replacements.keySet()) s = s.replace(r, replacements.get(r));
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
            }
        }
    }
    public ItemStack d(FileConfiguration config, String path) {
        item = null;
        if(config == null && path != null || config != null && config.get(path + ".item") != null) {
            if(config != null && config.getString(path + ".item").toLowerCase().contains("spawner") && !config.getString(path + ".item").toLowerCase().startsWith("mob_spawner")) {
                item = UMaterial.SPAWNER.getItemStack(); itemMeta = item.getItemMeta();
                if(config.get(path + ".name") != null) {
                    itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', config.getString(path + ".name")));
                    item.setItemMeta(itemMeta);
                }
                return item;
            }
            int amount = config != null && config.get(path + ".amount") != null ? config.getInt(path + ".amount") : 1;
            if(config == null && path.toLowerCase().contains(";amount=")) {
                amount = Integer.parseInt(path.toLowerCase().split(";amount=")[1]);
                path = path.split(";amount=")[0];
            }
            final ItemStack B = checkForCustomItem(config == null ? path : config.getString(path + ".item"));
            if(B != null) {
                item = B.clone();
                item.setAmount(amount);
                return item;
            }
            boolean enchanted = config != null && config.getBoolean(path + ".enchanted");
            lore.clear();
            String it = config != null ? config.getString(path + ".item").toUpperCase() : path, name = config != null ? config.getString(path + ".name") : null;
            final String material = it.toUpperCase();
            final UMaterial u;
            try {
                u = UMaterial.match(material);
                item = u.getItemStack();
            } catch (NullPointerException e) {
                Bukkit.broadcastMessage("Unrecognized material: " + material);
                return null;
            }
            final Material skullitem = UMaterial.PLAYER_HEAD_ITEM.getMaterial();
            item.setAmount(amount);
            itemMeta = item.getItemMeta();
            if(item.getType().equals(skullitem) && item.getData().getData() == 3) {
                ((SkullMeta) itemMeta).setOwner(it.split(":").length == 4 ? it.split(":")[3].split("}")[0] : "RandomHashTags");
            } else if(u.name().contains("SPAWN_EGG") && !version.contains("1.8")) {
                ((org.bukkit.inventory.meta.SpawnEggMeta) itemMeta).setSpawnedType(EntityType.valueOf(u.name().split("_SPAWN_EGG")[0]));
            }
            itemMeta.setDisplayName(name != null ? ChatColor.translateAlternateColorCodes('&', name) : null);

            if(enchanted) itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            final HashMap<Enchantment, Integer> enchants = new HashMap<>();
            if(config != null && config.get(path + ".lore") != null) {
                lore.clear();
                for(String string : config.getStringList(path + ".lore")) {
                    if(string.toLowerCase().startsWith("e:"))
                        enchants.put(getEnchantment(string.split(":")[1]), getRemainingInt(string));
                    else if(string.toLowerCase().startsWith("venchants{")) {
                        for(String s : string.split("\\{")[1].split("}")[0].split(";")) {
                            enchants.put(getEnchantment(s), getRemainingInt(s));
                        }
                    } else
                        lore.add(ChatColor.translateAlternateColorCodes('&', string));
                }
            }
            itemMeta.setLore(lore);
            item.setItemMeta(itemMeta);
            lore.clear();
            if(enchanted) item.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 1);
            for(Enchantment enchantment : enchants.keySet())
                if(enchantment != null)
                    item.addUnsafeEnchantment(enchantment, enchants.get(enchantment));
        }
        return item;
    }
    public ItemStack checkForCustomItem(String input) {
        final PlayerSkills ps = PlayerSkills.getPlayerSkills();
        final String Q = input.split(";")[0];
        input = input.toLowerCase();
        if(input.startsWith("itemrecipepage")) {
            final HashMap<String, ItemRecipe> L = ItemRecipe.recipes;
            final ItemRecipe i = L != null ? !input.contains(":") || Q.split(":")[1].equals("random") ? L.get(L.keySet().toArray()[random.nextInt(L.size())]) : L.getOrDefault(Q.split(":")[1], null) : null;
            return i != null ? i.getPage() : air;
        } else if(input.startsWith("itemrecipe")) {
            final HashMap<String, ItemRecipe> L = ItemRecipe.recipes;
            final ItemRecipe i = L != null ? !input.contains(":") || Q.split(":")[1].equals("random") ? L.get(L.keySet().toArray()[random.nextInt(L.size())]) : L.getOrDefault(Q.split(":")[1], null) : null;
            return i != null ? i.getItem() : air;
        } else if(input.startsWith("luckyblock")) {
            final HashMap<String, SkyKit> L = SkyKit.kits;
            final SkyKit k = L != null ? !input.contains(":") || Q.split(":")[1].equals("random") ? L.get(L.keySet().toArray()[random.nextInt(L.size())]) : L.getOrDefault(Q.split(":")[1], null) : null;
            return k != null ? k.getLuckyBlock() : air;
        } else if(input.startsWith("scrap:")) return Resource.valueOf(Q.split(":")[1], ResourceType.SCRAP).item();
        else if(input.startsWith("node:")) {
            return ResourceNode.paths.getOrDefault(Q.split(":")[1], null).item();
        } else if(input.startsWith("pet")) {
            final HashMap<String, Pet> p = Pet.pets;
            final Pet pet = p != null ? !input.contains(":") || Q.split(":")[1].equals("random") ? p.get(p.keySet().toArray()[random.nextInt(p.size())]) : p.getOrDefault(Q.split(":")[1], null) : null;
            return pet != null ? pet.getItem() : air;
        } else if(input.startsWith("resource:")) return Resource.valueOf(Q.split(":")[1], ResourceType.RESOURCE).item();
        else if(input.startsWith("resourceitem:")) return Resource.valueOf(Q.split(":")[1], ResourceType.RESOURCE_ITEM).item();
        else if(input.startsWith("resourcefragment:")) return Resource.valueOf(Q.split(":")[1], ResourceType.FRAGMENT).item();
        else if(input.startsWith("permissionblock:")) return PermissionBlock.paths.get(Q.split(":")[1]).item();
        else if(input.startsWith("repairscroll:")) return RepairScroll.get(Q.split(":")[1], Integer.parseInt(Q.split(":")[2]));
        else if(input.startsWith("lootbox:")) return input.split(":")[1].equals("latest") ? Lootbox.latest().item() : Lootbox.valueOF(Q.split(":")[1].split("\\.")[0] + ".yml").item();
        else if(input.startsWith("adventuremap:")) return Adventure.adventures.get(Q.split(":")[1]).getMap();
        else if(input.startsWith("adventuremapfragment:")) return Adventure.adventures.get(Q.split(":")[1]).getFragment();
        else if(input.startsWith("farmingrecipe:")) return FarmingRecipe.paths.getOrDefault(Q.split(":")[1], null).item();
        else if(input.startsWith("xpbottle:") || input.startsWith("banknote:") || input.startsWith("itemlorecrystal") || input.startsWith("flightorb") || input.startsWith("itemnametag") || input.startsWith("spacedrink") || input.startsWith("totemofundying")) {
            return GivedpItem.getGivedpItem().valueOf(Q);
        } else if(input.startsWith("skilltoken")) return ps.token.clone();
        else if(input.startsWith("skillshard")) return ps.shard.clone();
        else if(input.startsWith("skitgem")) {
            final HashMap<String, SkyKit> L = SkyKit.kits;
            final SkyKit k = L != null ? !input.contains(":") || Q.split(":")[1].equals("random") ? L.get(L.keySet().toArray()[random.nextInt(L.size())]) : L.getOrDefault(Q.split(":")[1], null) : null;
            return k != null ? k.getGem() : air;
        } else if(input.startsWith("shield:")) {
            return CustomShield.paths.get(Q.split(":")[1]).item();
        } else if(input.startsWith("bow:")) {
            return CustomBow.paths.get(Q.split(":")[1]).item();
        } else if(input.startsWith("playerrank:")) return PlayerRank.paths.get(Q.split(":")[1]).item();
        else if(input.contains("spawner") && !input.equals("mob_spawner")) return getSpawner(input);
        else if(input.startsWith("collectionchest")) return CollectionChests.getCollectionChests().collectionchest.clone();
        else if(customitems != null && customitems.containsKey(Q)) return customitems.get(Q).clone();
        return null;
    }
    public Object getEpicSpawnerData(String entitytype) {
        Object data = null;
        if(epicSpawners) {
            for(com.songoda.epicspawners.api.spawner.SpawnerData spawnerData : com.songoda.epicspawners.EpicSpawnersPlugin.getInstance().getSpawnerManager().getAllSpawnerData()) {
                final String input = entitytype.toUpperCase().replace("_", "").replace(" ", ""), compare = spawnerData.getIdentifyingName().toUpperCase().replace("_", "").replace(" ", "");
                if(input.equals(compare))
                    data = spawnerData;
            }
        }
        return data;
    }
    public ItemStack getSpawner(String input) {
        String pi = input.toLowerCase(), type = null;
        for(EntityType entitytype : EntityType.values()) {
            if(pi.equalsIgnoreCase(entitytype.name().replace("_", "") + "spawner")
                    || pi.endsWith(entitytype.name().toLowerCase().replace("_", "-")))
                type = entitytype.name().toLowerCase().replace("_", "");
        }
        if(type == null) {
            if(pi.contains("pig") && pi.contains("zombie")) type = "pigzombie";
        }
        if(type == null) return null;

        if(pluginmanager.isPluginEnabled("EpicSpawners")) {
            com.songoda.epicspawners.api.spawner.SpawnerData data = (com.songoda.epicspawners.api.spawner.SpawnerData) getEpicSpawnerData(type);
            return data.toItemStack();
        } else {
            sendConsoleMessage("&6[RandomSky] &cThe plugin EpicSpawners is required to use this feature!");
        }
        return null;
    }
}
