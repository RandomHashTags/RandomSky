package me.randomhashtags.randomsky.api.events.island;

import me.randomhashtags.randomsky.utils.classes.island.Island;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

public class IslandPlaceBlockEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;

    public final Player player;
    public final Island island;
    public final ItemStack item;
    public final Block block;
    public IslandPlaceBlockEvent(Player player, Island island, ItemStack item, Block block) {
        this.player = player;
        this.island = island;
        this.item = item;
        this.block = block;
    }
    public boolean isCancelled() { return cancelled; }
    public void setCancelled(boolean cancel) { cancelled = cancel; }
    public HandlerList getHandlers() { return handlers; }
    public static HandlerList getHandlerList() { return handlers; }
}
