package me.randomhashtags.randomsky.api.events.island;

import me.randomhashtags.randomsky.utils.classes.island.Island;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class IslandCreateEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;
    public final long creationTime;
    public final Player creator;
    public final Island island;
    public IslandCreateEvent(long creationTime, Player creator, Island island) {
        this.creationTime = creationTime;
        this.creator = creator;
        this.island = island;
    }
    public boolean isCancelled() { return cancelled; }
    public void setCancelled(boolean cancel) { cancelled = cancel; }
    public HandlerList getHandlers() { return handlers; }
    public static HandlerList getHandlerList() { return handlers; }
}
