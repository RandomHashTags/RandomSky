package me.randomhashtags.randomsky.api.events.island;

import me.randomhashtags.randomsky.utils.classes.island.Island;
import me.randomhashtags.randomsky.utils.classes.resources.ActiveResourceNode;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

public class HarvestResourceNodeEvent extends Event implements Listener {
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;

    public final Player player;
    public final Island island;
    public final ActiveResourceNode node;
    public HarvestResourceNodeEvent(Player player, Island island, ActiveResourceNode node) {
        this.player = player;
        this.island = island;
        this.node = node;
    }
    public boolean isCancelled() { return cancelled; }
    public void setCancelled(boolean cancel) { cancelled = cancel; }
    public HandlerList getHandlers() { return handlers; }
    public static HandlerList getHandlerList() { return handlers; }
}
