package me.randomhashtags.randomsky.api.events;

import me.randomhashtags.randomsky.utils.classes.island.Island;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.BlockBreakEvent;

public class PlayerIslandBreakBlockEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;

    public final Player player;
    public final Island island;
    public final BlockBreakEvent breakEvent;
    public PlayerIslandBreakBlockEvent(Player player, Island island, BlockBreakEvent breakEvent) {
        this.player = player;
        this.island = island;
        this.breakEvent = breakEvent;
    }
    public boolean isCancelled() { return cancelled; }
    public void setCancelled(boolean cancel) { cancelled = cancel; }
    public HandlerList getHandlers() { return handlers; }
    public static HandlerList getHandlerList() { return handlers; }
}
