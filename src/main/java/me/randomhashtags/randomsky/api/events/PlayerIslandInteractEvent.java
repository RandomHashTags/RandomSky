package me.randomhashtags.randomsky.api.events;

import me.randomhashtags.randomsky.utils.classes.island.Island;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerIslandInteractEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;

    public final Player player;
    public final Island island;
    public final PlayerInteractEvent interactEvent;
    public PlayerIslandInteractEvent(Player player, Island island, PlayerInteractEvent interactEvent) {
        this.player = player;
        this.island = island;
        this.interactEvent = interactEvent;
    }
    public boolean isCancelled() { return cancelled; }
    public void setCancelled(boolean cancel) { cancelled = cancel; }
    public HandlerList getHandlers() { return handlers; }
    public static HandlerList getHandlerList() { return handlers; }
}