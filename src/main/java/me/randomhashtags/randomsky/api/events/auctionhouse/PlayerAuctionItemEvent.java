package me.randomhashtags.randomsky.api.events.auctionhouse;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

public class PlayerAuctionItemEvent extends Event implements Cancellable {
	private static final HandlerList handlers = new HandlerList();
	private boolean cancelled;
	public final Player player;
	public final long auctionedtime;
	public final ItemStack item;
	public final double price;
	public PlayerAuctionItemEvent(Player player, long auctionedtime, ItemStack item, double price) {
		this.player = player;
		this.auctionedtime = auctionedtime;
		this.item = item;
		this.price = price;
		cancelled = false;
	}
	public boolean isCancelled() { return cancelled; }
	public void setCancelled(boolean cancel) { cancelled = cancel; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
}