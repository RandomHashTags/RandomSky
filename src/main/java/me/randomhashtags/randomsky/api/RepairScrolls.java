package me.randomhashtags.randomsky.api;

import me.randomhashtags.randomsky.RandomSkyAPI;
import me.randomhashtags.randomsky.utils.classes.RepairScroll;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.io.File;

public class RepairScrolls extends RandomSkyAPI implements Listener {

    private static RepairScrolls instance;
    public static RepairScrolls getRepairScrolls() {
        if(instance == null) instance = new RepairScrolls();
        return instance;
    }

    public boolean isEnabled = false;
    public YamlConfiguration config;

    public void enable() {
        final long started = System.currentTimeMillis();
        if(isEnabled) return;
        save(null, "repair scrolls.yml");
        config = YamlConfiguration.loadConfiguration(new File(randomsky.getDataFolder(), "repair scrolls.yml"));
        pluginmanager.registerEvents(this, randomsky);
        isEnabled = true;
        int loaded = 0;
        for(String path : config.getConfigurationSection("repair scrolls").getKeys(false)) {
            final String s = "repair scrolls." + path + ".";
            new RepairScroll(path, d(config, "repair scrolls." + path), config.getStringList(s + "applyable to"));
            loaded++;
        }
        sendConsoleMessage("&6[RandomSky] &aLoaded " + loaded + " Repair Scrolls &e(took " + (System.currentTimeMillis()-started) + "ms)");
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        config = null;
        RepairScroll.scrolls.clear();
        HandlerList.unregisterAll(this);
    }

    @EventHandler
    private void inventoryClickEvent(InventoryClickEvent event) {
        final ItemStack current = event.getCurrentItem();
        ItemStack cursor = event.getCursor();
        if(!event.isCancelled() && current != null && !current.getType().equals(Material.AIR) && cursor != null && !cursor.getType().equals(Material.AIR)) {
            final RepairScroll r = RepairScroll.valueOf(cursor);
            final Player player = (Player) event.getWhoClicked();
            if(r != null) {
                event.setCancelled(true);
                if(r.canBeAppliedTo(current)) {
                    final double percent = r.getPercent(cursor)/100.00;
                    final short d = current.getDurability(), max = current.getType().getMaxDurability(), repaired = (short) (max*percent), n = (short) (d-repaired < 0 ? 0 : d-repaired);
                    current.setDurability(n);
                    final int amount = cursor.getAmount();
                    if(amount == 1) {
                        cursor = new ItemStack(Material.AIR);
                    } else {
                        cursor.setAmount(amount-1);
                    }
                    event.setCursor(cursor);
                    event.setCurrentItem(current);
                } else {
                    sendStringListMessage(player, config.getStringList("messages.cannot repair item with scroll"), null);
                }
                player.updateInventory();
            }
        }
    }
    @EventHandler
    private void playerInteractEvent(PlayerInteractEvent event) {
        final RepairScroll scroll = RepairScroll.valueOf(event.getItem());
        if(scroll != null && event.getAction().name().contains("RIGHT")) {
            event.setCancelled(true);
            event.getPlayer().updateInventory();
        }
    }
}
