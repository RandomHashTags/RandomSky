package me.randomhashtags.randomsky.api;

import me.randomhashtags.randomsky.RandomSkyAPI;
import me.randomhashtags.randomsky.utils.classes.kits.SkyKit;
import me.randomhashtags.randomsky.utils.universal.UInventory;
import org.bukkit.command.CommandExecutor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import java.io.File;
import java.util.HashMap;

public class SkyKits extends RandomSkyAPI implements Listener, CommandExecutor {

    private static SkyKits instance;
    public static final SkyKits getSkyKits() {
        if(instance == null) instance = new SkyKits();
        return instance;
    }

    public boolean isEnabled = false;
    public YamlConfiguration config;

    private UInventory gui;

    public void enable() {
        final long started = System.currentTimeMillis();
        if(isEnabled) return;
        save(null, "sky kits.yml");
        pluginmanager.registerEvents(this, randomsky);
        isEnabled = true;

        config = YamlConfiguration.loadConfiguration(new File(rsd, "sky kits.yml"));

        if(!otherdata.getBoolean("saved default sky kits")) {
            final String[] a = new String[]{"EXPLORER"};
            for(String s : a) save("sky kits", s + ".yml");
            otherdata.set("saved default sky kits", true);
            saveOtherData();
        }

        for(File f : new File(rsd + separator + "sky kits").listFiles()) {
            new SkyKit(f);
        }
        final HashMap<String, SkyKit> k = SkyKit.kits;
        sendConsoleMessage("&6[RandomSky] &aLoaded " + (k != null ? k.size() : 0) + " Sky Kits &e(took " + (System.currentTimeMillis()-started) + "ms)");
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        config = null;
        SkyKit.deleteAll();
        HandlerList.unregisterAll(this);
    }
}
