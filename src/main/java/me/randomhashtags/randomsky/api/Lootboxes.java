package me.randomhashtags.randomsky.api;

import me.randomhashtags.randomsky.RandomSkyAPI;
import me.randomhashtags.randomsky.utils.classes.Lootbox;
import me.randomhashtags.randomsky.utils.universal.UInventory;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Lootboxes extends RandomSkyAPI implements Listener, CommandExecutor {

    private static Lootboxes instance;
    public static final Lootboxes getLootboxes() {
        if(instance == null) instance = new Lootboxes();
        return instance;
    }

    public boolean isEnabled = false;
    public YamlConfiguration config;

    private UInventory gui;
    private HashMap<Lootbox, Long> started;
    private int countdownStart = 0;
    private ItemStack background;
    private List<String> opened, rewardFormat;
    private HashMap<Integer, Lootbox> guiLootboxes;
    private HashMap<Player, Lootbox> redeeming;
    private HashMap<Player, List<Integer>> tasks;

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        final Player player = sender instanceof Player ? (Player) sender : null;
        if(player != null) {
            viewLootbox(player);
        }
        return true;
    }

    public void enable() {
        final long sc = System.currentTimeMillis();
        if(isEnabled) return;
        save(null, "lootboxes.yml");
        config = YamlConfiguration.loadConfiguration(new File(randomsky.getDataFolder(), "lootboxes.yml"));
        pluginmanager.registerEvents(this, randomsky);
        isEnabled = true;

        started = new HashMap<>();
        countdownStart = config.getInt("settings.countdown start");
        opened = colorizeListString(config.getStringList("messages.opened"));
        rewardFormat = colorizeListString(config.getStringList("messages.reward format"));

        guiLootboxes = new HashMap<>();
        redeeming = new HashMap<>();
        tasks = new HashMap<>();

        final String title = ChatColor.translateAlternateColorCodes('&', config.getString("gui.title")), type = config.getString("gui.type");
        final int size = config.getInt("gui.size");
        if(type != null) {
            final InventoryType i = InventoryType.valueOf(type);
            gui = new UInventory(null, i, title);
        } else {
            gui = new UInventory(null, size, title);
        }
        background = d(config, "gui.background");

        final YamlConfiguration a = otherdata;
        if(!a.getBoolean("saved default lootboxes")) {
            final String[] lb = new String[] {
                "CONQUEST", "SPECIALTY"
            };
            for(String l : lb) save("lootboxes", l + ".yml");
            a.set("saved default lootboxes", true);
            saveOtherData();
        }
        final File F = rsd;
        final String sep = separator;
        for(File f : new File(randomsky.getDataFolder() + sep + "lootboxes").listFiles()) {
            final String n = f.getName();
            new Lootbox(YamlConfiguration.loadConfiguration(new File(F + sep + "lootboxes", n)), n);
        }

        final Inventory gi = gui.getInventory();
        for(String s : config.getConfigurationSection("gui").getKeys(false)) {
            if(!s.equals("title") && !s.equals("type") && !s.equals("size") && !s.equals("background")) {
                final ItemStack is = d(config, "gui." + s);
                final int slot = config.getInt("gui." + s + ".slot");
                final Lootbox l = Lootbox.valueOf(is);
                if(l != null) guiLootboxes.put(slot, l);
                gi.setItem(slot, is);
            }
        }
        for(int i = 0; i < size; i++) {
            item = gi.getItem(i);
            if(item == null) gi.setItem(i, background);
        }

        final ConfigurationSection c = a.getConfigurationSection("lootboxes.started");
        if(c == null) {
            for(int i : guiLootboxes.keySet()) {
                started.put(guiLootboxes.get(i), System.currentTimeMillis());
            }
        } else {
            for(String s : c.getKeys(false)) {
                started.put(Lootbox.valueOF(s + ".yml"), a.getLong("lootboxes.started." + s));
            }
        }

        sendConsoleMessage("&6[RandomSky] &aLoaded " + Lootbox.lootboxes.size() + " lootboxes &e(took " + (System.currentTimeMillis()-sc) + "ms)");
    }
    public void disable() {
        if(!isEnabled) return;
        final YamlConfiguration a = otherdata;
        for(Lootbox l : started.keySet()) {
            a.set("lootboxes.started." + l.ymlName.split("\\.yml")[0], started.get(l));
        }
        saveOtherData();
        for(Player p : redeeming.keySet()) p.closeInventory();
        for(Player p : tasks.keySet()) p.closeInventory();
        config = null;
        gui = null;
        background = null;
        opened = null;
        rewardFormat = null;
        guiLootboxes = null;
        redeeming = null;
        tasks = null;
        started = null;
        isEnabled = false;
        Lootbox.deleteAll();
        HandlerList.unregisterAll(this);
    }

    public void viewLootbox(Player player) {
        if(hasPermission(player, "RandomSky.lootbox.view", true)) {
            player.closeInventory();
            final int size = gui.getSize();
            final long time = System.currentTimeMillis();
            player.openInventory(Bukkit.createInventory(player, gui.getType(), gui.getTitle()));
            final Inventory top = player.getOpenInventory().getTopInventory();
            top.setContents(gui.getInventory().getContents());
            for(int i = 0; i < size; i++) {
                item = top.getItem(i);
            }
            final List<String> a = colorizeListString(config.getStringList("lores.available")), e = colorizeListString(config.getStringList("lores.expired")), p = colorizeListString(config.getStringList("lores.preview"));
            for(int i : guiLootboxes.keySet()) {
                final Lootbox l = guiLootboxes.get(i);
                final long L = started.get(l), ex = L+l.availableFor*1000;
                final String n = l.name;
                item = top.getItem(i); itemMeta = item.getItemMeta(); lore.clear();
                lore.addAll(itemMeta.getLore());
                for(String s : time < ex ? a : e) {
                    lore.add(s.replace("{NAME}", n).replace("{TIME}", getRemainingTime(ex-time)));
                }
                lore.addAll(p);
                itemMeta.setLore(lore); lore.clear();
                item.setItemMeta(itemMeta);
            }
            player.updateInventory();
        }
    }
    public boolean isAvailable(Lootbox lootbox) {
        return started.containsKey(lootbox) && started.get(lootbox)-System.currentTimeMillis() > 0;
    }
    public void tryClaiming(Player player, Lootbox lootbox) {
        if(hasPermission(player, "RandomSky.lootbox." + lootbox.ymlName.split("\\.yml")[0], false)) {
            if(isAvailable(lootbox)) {
                giveItem(player, lootbox.item());
            }
        } else {
            sendStringListMessage(player, config.getStringList("messages.unlock"), null);
        }
        player.closeInventory();
    }
    public void openLootbox(Player player, Lootbox lootbox) {
        player.closeInventory();
        final int size = lootbox.guiSize;
        player.openInventory(Bukkit.createInventory(player, size, lootbox.guiTitle));
        final List<String> format = lootbox.guiFormat;
        final ItemStack background = lootbox.background();
        final Inventory top = player.getOpenInventory().getTopInventory();
        final List<Integer> countdownSlots = new ArrayList<>(), lootSlots = new ArrayList<>(), bonusSlots = new ArrayList<>();
        final List<ItemStack> bonus = lootbox.bonusLoot(), regular = lootbox.regularLoot();
        redeeming.put(player, lootbox);
        for(int i = 0; i < format.size(); i++) {
            final String L = format.get(i);
            for(int p = 0; p < L.length(); p++) {
                final int slot = i*9+p;
                final String s = L.substring(p, p+1);
                if(s.equals("X"))  {
                    top.setItem(slot, background);
                } else if(s.equals("C")) {
                    item = background.clone();
                    item.setAmount(countdownStart);
                    top.setItem(slot, item);
                    countdownSlots.add(slot);
                } else if(s.equals("B")) {
                    final ItemStack b = bonus.get(random.nextInt(bonus.size()));
                    top.setItem(slot, b);
                    bonus.remove(b);
                    bonusSlots.add(slot);
                } else if(s.equals("L")) {
                    final ItemStack r = regular.get(random.nextInt(regular.size()));
                    top.setItem(slot, r);
                    regular.remove(r);
                    lootSlots.add(slot);
                }
            }
        }
        tasks.put(player, new ArrayList<>());
        tasks.get(player).add(scheduler.scheduleSyncDelayedTask(randomsky, () -> startCountdown(player, top, countdownSlots, lootSlots, bonusSlots, background), 10));
        player.updateInventory();
    }
    private void startCountdown(Player player, Inventory top, List<Integer> countdownSlots, List<Integer> lootSlots, List<Integer> bonusSlots, ItemStack background) {
        final Lootbox l = redeeming.get(player);
        final ItemStack air = new ItemStack(Material.AIR);
        final List<Integer> T = tasks.get(player);
        for(int i = 1; i <= countdownStart; i++) {
            final int k = i;
            T.add(scheduler.scheduleSyncDelayedTask(randomsky, () -> {
                item = background.clone();
                item.setAmount(countdownStart-k);
                for(int c : countdownSlots) {
                    top.setItem(c, item);
                }
                if(k == countdownStart) {
                    for(int n = 0; n < top.getSize(); n++) {
                        if(!lootSlots.contains(n) && !bonusSlots.contains(n)) {
                            top.setItem(n, air);
                        }
                    }
                } else {
                    for(int z = 0; z <= 18; z += 3) {
                        T.add(scheduler.scheduleSyncDelayedTask(randomsky, () -> {
                            final List<ItemStack> loot = l.regularLoot();
                            for(int s : lootSlots) {
                                final ItemStack r = loot.get(random.nextInt(loot.size()));
                                loot.remove(r);
                                top.setItem(s, r);
                            }
                        }, z));
                    }
                }
                player.updateInventory();
            }, 20*i));
        }
    }
    public void previewLootbox(Player player, Lootbox lootbox) {
        if(hasPermission(player, "RandomSky.lootbox.preview", true)) {
            player.closeInventory();
            final List<ItemStack> items = lootbox.items();
            final int s = ((items.size()+9)/9)*9;
            player.openInventory(Bukkit.createInventory(player, s, lootbox.previewTitle));
            final Inventory top = player.getOpenInventory().getTopInventory();
            for(ItemStack is : items) top.setItem(top.firstEmpty(), is);
            player.updateInventory();
        }
    }

    @EventHandler
    private void inventoryClickEvent(InventoryClickEvent event) {
        if(!event.isCancelled()) {
            final Player player = (Player) event.getWhoClicked();
            final Inventory top = player.getOpenInventory().getTopInventory();
            final String t = top.getTitle();
            final Lootbox l = Lootbox.valueOf(t), L = l == null ? Lootbox.valueof(t) : null;
            if(redeeming.containsKey(player)) {
                event.setCancelled(true);
                player.updateInventory();
            } else if(top.getHolder() == player && (l != null || L != null || t.equals(gui.getTitle()))) {
                event.setCancelled(true);
                player.updateInventory();
                final ItemStack c = event.getCurrentItem();
                final int r = event.getRawSlot();
                if(r < 0 || r >= top.getSize() || c == null || c.getType().equals(Material.AIR)) return;
                if(t.equals(gui.getTitle())) {
                    final Lootbox lootbox = guiLootboxes.getOrDefault(r, null);
                    final String click = event.getClick().name();
                    if(lootbox != null) {
                        if(click.contains("LEFT")) tryClaiming(player, lootbox);
                        else if(click.contains("RIGHT")) previewLootbox(player, lootbox);
                    }
                } else if(L != null) {
                    player.closeInventory();
                    sendStringListMessage(player, config.getStringList("messages.unlock"), null);
                } else {

                }
            }
        }
    }
    @EventHandler
    private void playerInteractEvent(PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final ItemStack i = event.getItem();
        final Lootbox l = Lootbox.valueOf(i);
        if(l != null && event.getAction().name().contains("RIGHT")) {
            event.setCancelled(true);
            player.updateInventory();
            removeItem(player, i ,1);
            openLootbox(player, l);
        }
    }
    @EventHandler
    private void inventoryCloseEvent(InventoryCloseEvent event) {
        final Player player = (Player) event.getPlayer();
        if(redeeming.containsKey(player)) {
            final Inventory i = event.getInventory();
            final Lootbox l = redeeming.get(player);
            final ItemStack background = l.background();
            final List<ItemStack> rewards = new ArrayList<>();
            for(ItemStack is : i.getContents()) {
                if(is != null && !is.getType().equals(Material.AIR) && !is.isSimilar(background)) {
                    rewards.add(is);
                    giveItem(player, is);
                }
            }
            final String p = player.getName(), lb = l.name;
            for(String s : opened) {
                if(s.equals("{REWARDS}")) {
                    for(ItemStack is : rewards) {
                        final String amount = Integer.toString(is.getAmount()), itemname = is.hasItemMeta() && is.getItemMeta().hasDisplayName() ? is.getItemMeta().getDisplayName() : toMaterial(is.getType().name(), false);
                        for(String m : rewardFormat) {
                            Bukkit.broadcastMessage(m.replace("{AMOUNT}", amount).replace("{ITEM_NAME}", itemname));
                        }
                    }
                } else {
                    Bukkit.broadcastMessage(s.replace("{PLAYER}", p).replace("{NAME}", lb));
                }
            }
            redeeming.remove(player);
            for(int task : tasks.get(player)) {
                scheduler.cancelTask(task);
            }
            tasks.remove(player);
        }
    }
}
