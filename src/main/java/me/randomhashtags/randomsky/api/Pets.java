package me.randomhashtags.randomsky.api;

import me.randomhashtags.randomsky.RandomSkyAPI;
import me.randomhashtags.randomsky.utils.classes.Pet;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import java.io.File;
import java.util.HashMap;

public class Pets extends RandomSkyAPI implements Listener {

    private static Pets instance;
    public static Pets getPets() {
        if(instance == null) instance = new Pets();
        return instance;
    }

    public boolean isEnabled = false;

    public void enable() {
        final long started = System.currentTimeMillis();
        if(isEnabled) return;
        pluginmanager.registerEvents(this, randomsky);
        isEnabled = true;

        if(!otherdata.getBoolean("saved default pets")) {
            final String[] a = new String[]{"BATTLE PIG", "FARMER BOB"};
            for(String s : a) save("pets", s + ".yml");
            otherdata.set("saved default pets", true);
            saveOtherData();
        }

        for(File f : new File(rsd + separator + "pets").listFiles()) {
            new Pet(f);
        }
        final HashMap<String, Pet> p = Pet.pets;
        sendConsoleMessage("&6[RandomSky] &aLoaded " + (p != null ? p.size() : 0) + " Pets &e(took " + (System.currentTimeMillis()-started) + "ms)");
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        Pet.deleteAll();
        HandlerList.unregisterAll(this);
    }
}
