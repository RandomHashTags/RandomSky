package me.randomhashtags.randomsky.api;

import me.randomhashtags.randomsky.RandomSkyAPI;
import me.randomhashtags.randomsky.utils.RSPlayer;
import me.randomhashtags.randomsky.utils.classes.adventures.Adventure;
import me.randomhashtags.randomsky.utils.universal.UInventory;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Adventures extends RandomSkyAPI implements Listener, CommandExecutor {

    private static Adventures instance;
    public static final Adventures getAdventures() {
        if(instance == null) instance = new Adventures();
        return instance;
    }

    public boolean isEnabled = false;
    public YamlConfiguration config;

    private UInventory gui;
    private ItemStack mapRequired;
    private List<World> worlds;

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        final Player player = sender instanceof Player ? (Player) sender : null;
        final int l = args.length;
        if(l == 0) {
            viewAdventures(player);
        } else {
            final String a = args[0];
            if(a.equals("help")) {
                viewHelp(sender);
            }
        }
        return true;
    }

    public void enable() {
        final long started = System.currentTimeMillis();
        if(isEnabled) return;
        save(null, "adventures.yml");
        if(!otherdata.getBoolean("saved default adventures")) {
            save("adventures", "ABANDONED_RUINS.yml");
            save("adventures", "DEMONIC_REALM.yml");
            save("adventures", "LOST_WASTELAND.yml");
            otherdata.set("saved default adventures", true);
            saveOtherData();
        }
        config = YamlConfiguration.loadConfiguration(new File(randomsky.getDataFolder(), "adventures.yml"));
        pluginmanager.registerEvents(this, randomsky);
        isEnabled = true;

        worlds = new ArrayList<>();

        mapRequired = d(config, "gui.map required");
        gui = new UInventory(null, config.getInt("gui.size"), ChatColor.translateAlternateColorCodes('&', config.getString("gui.title")));
        final Inventory gi = gui.getInventory();
        final ItemStack b = d(config, "gui.background");
        final String s = separator;
        for(File f : new File(randomsky.getDataFolder() + s + "adventures").listFiles()) {
            final Adventure a = new Adventure(f);
            gi.setItem(a.slot, a.display());
            worlds.add(a.getTeleportLocations().get(0).getWorld());
        }

        for(Adventure a : Adventure.adventures.values()) {
            final String r = a.getYaml().getString("map found in");
            a.mapFoundIn = r != null ? Adventure.adventures.getOrDefault(r, null) : null;
        }
        for(int i = 0; i < gui.getSize(); i++)
            if(gi.getItem(i) == null)
                gi.setItem(i, b);
        final HashMap<String, Adventure> a = Adventure.adventures;
        sendConsoleMessage("&6[RandomSky] &aLoaded " + (a != null ? a.size() : 0) + " Adventures &e(took " + (System.currentTimeMillis()-started) + "ms)");
    }
    public void disable() {
        if(!isEnabled) return;
        Adventure.deleteAll();
        isEnabled = false;
        config = null;
        gui = null;
        mapRequired = null;
        worlds = null;
        HandlerList.unregisterAll(this);
    }

    public void viewHelp(CommandSender sender) {
        if(hasPermission(sender, "RandomSky.adventure.help", true)) {
            sendStringListMessage(sender, config.getStringList("messages.help"), null);
        }
    }

    public void viewAdventures(Player player) {
        if(hasPermission(player, "RandomSky.adventures.view", true)) {
            final List<Adventure> allowed = RSPlayer.get(player.getUniqueId()).getAllowedAdventures();
            final int size = gui.getSize();
            player.openInventory(Bukkit.createInventory(player, size, gui.getTitle()));
            final Inventory top = player.getOpenInventory().getTopInventory();
            top.setContents(gui.getInventory().getContents());
            for(int i = 0; i < size; i++) {
                item = top.getItem(i);
                if(item != null) {
                    final Adventure a = Adventure.valueOf(i);
                    if(a != null) {
                        final String max = Integer.toString(a.maxPlayers), online = Integer.toString(a.getPlayers().size());
                        itemMeta = item.getItemMeta(); lore.clear();
                        for(String s : itemMeta.getLore()) {
                            lore.add(s.replace("{ONLINE}", online).replace("{MAX}", max));
                        }
                        itemMeta.setLore(lore); lore.clear();
                        item.setItemMeta(itemMeta);
                        if(a.mapFoundIn != null && !allowed.contains(a)) {
                            final ItemStack s = mapRequired.clone();
                            itemMeta = s.getItemMeta(); lore.clear();
                            itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{NAME}", item.getItemMeta().getDisplayName()));
                            lore.addAll(item.getItemMeta().getLore());
                            final String r = a.mapFoundIn.getYamlName();
                            for(String l : itemMeta.getLore())
                                lore.add(l.replace("{REQ_ADVENTURE}", r));
                            itemMeta.setLore(lore); lore.clear();
                            item = s;
                            item.setItemMeta(itemMeta);
                        }
                        itemMeta = item.getItemMeta();
                        itemMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                        item.setItemMeta(itemMeta);
                        top.setItem(i, item);
                    }
                }
            }
            player.updateInventory();
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    private void blockBreakEvent(BlockBreakEvent event) {
        if(!event.isCancelled()) {
            final Player player = event.getPlayer();
            if(worlds.contains(player.getWorld())) {
                event.setCancelled(true);
                player.updateInventory();
                sendStringListMessage(player, config.getStringList("messages.cannot break blocks"), null);
            }
        }
    }
    @EventHandler(priority = EventPriority.LOW)
    private void blockPlaceEvent(BlockPlaceEvent event) {
        if(!event.isCancelled()) {
            final Player player = event.getPlayer();
            if(worlds.contains(player.getWorld())) {
                event.setCancelled(true);
                player.updateInventory();
                sendStringListMessage(player, config.getStringList("messages.cannot place blocks"), null);
            }
        }
    }

    @EventHandler
    private void playerInteractEvent(PlayerInteractEvent event) {
        final ItemStack i = event.getItem();
        if(i != null) {
            final Adventure a = Adventure.valueOfMap(i);
            if(a != null) {
                final Player player = event.getPlayer();
                event.setCancelled(true);
                player.updateInventory();

                final RSPlayer pdata = RSPlayer.get(player.getUniqueId());
                final List<Adventure> allowed = pdata.getAllowedAdventures();
                if(!allowed.contains(a)) {
                    allowed.add(a);
                    removeItem(player, i, 1);
                    final HashMap<String, String> replacements = new HashMap<>();
                    replacements.put("{ADVENTURE}", a.getName());
                    sendStringListMessage(player, config.getStringList("messages.unlocked access"), replacements);
                }
            }
        }
    }

    @EventHandler
    private void inventoryClickEvent(InventoryClickEvent event) {
        if(!event.isCancelled()) {
            final Player player = (Player) event.getWhoClicked();
            final String t = event.getView().getTitle();
            if(t != null && t.equals(gui.getTitle())) {
                event.setCancelled(true);
                player.updateInventory();
                final ItemStack c = event.getCurrentItem();
                final int r = event.getRawSlot();
                if(r < 0 || r >= player.getOpenInventory().getTopInventory().getSize() || c == null || c.getType().equals(Material.AIR)) return;
                final Adventure a = Adventure.valueOf(r);
                if(a != null) {
                    final RSPlayer pdata = RSPlayer.get(player.getUniqueId());
                    if(a.getMap() != null && !pdata.getAllowedAdventures().contains(a)) {
                        final HashMap<String, String> replacements = new HashMap<>();
                        replacements.put("{ADVENTURE}", a.getName());
                        sendStringListMessage(player, config.getStringList("messages.need map to travel"), replacements);
                    } else {
                        final ItemStack[] inv = player.getInventory().getContents();
                        a.join(player);
                    }
                    player.updateInventory();
                }
            }
        }
    }
}
