package me.randomhashtags.randomsky.api;

import me.randomhashtags.randomsky.RandomSkyAPI;
import me.randomhashtags.randomsky.utils.classes.customenchants.CustomEnchant;
import me.randomhashtags.randomsky.utils.classes.customenchants.EnchantRarity;
import me.randomhashtags.randomsky.utils.universal.UInventory;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import java.io.File;

public class CustomEnchants extends RandomSkyAPI implements Listener {

    private static CustomEnchants instance;
    public static final CustomEnchants getCustomEnchants() {
        if(instance == null) instance = new CustomEnchants();
        return instance;
    }

    public boolean isEnabled = false;
    public YamlConfiguration config;
    private UInventory gemForgeExamine, gemForgeApply, gemForgeRemove;

    public void enable() {
        if(isEnabled) return;
        save(null, "custom enchants.yml");
        config = YamlConfiguration.loadConfiguration(new File(randomsky.getDataFolder(), "custom enchants.yml"));
        pluginmanager.registerEvents(this, randomsky);
        isEnabled = true;
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        EnchantRarity.rarities.clear();
        CustomEnchant.disabledEnchants.clear();
        CustomEnchant.enabledEnchants.clear();
        config = null;
        gemForgeExamine = null;
        gemForgeApply = null;
        gemForgeRemove = null;
        HandlerList.unregisterAll(this);
    }
}
