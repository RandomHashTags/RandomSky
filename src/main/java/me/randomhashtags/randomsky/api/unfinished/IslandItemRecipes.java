package me.randomhashtags.randomsky.api.unfinished;

import me.randomhashtags.randomsky.RandomSkyAPI;
import me.randomhashtags.randomsky.utils.classes.island.ItemRecipe;
import me.randomhashtags.randomsky.utils.universal.UInventory;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import java.io.File;

public class IslandItemRecipes extends RandomSkyAPI implements Listener, CommandExecutor {

    private static IslandItemRecipes instance;
    public static final IslandItemRecipes getIslandItemRecipes() {
        if(instance == null) instance = new IslandItemRecipes();
        return instance;
    }

    public boolean isEnabled = false;
    public YamlConfiguration config;

    private UInventory gui;

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        final Player player = sender instanceof Player ? (Player) sender : null;
        return true;
    }

    public void enable() {
        final long started = System.currentTimeMillis();
        if(isEnabled) return;
        save(null, "island farming.yml");
        config = YamlConfiguration.loadConfiguration(new File(rsd, "island farming.yml"));
        pluginmanager.registerEvents(this, randomsky);
        isEnabled = true;

        int loaded = 0;
        for(String s : config.getConfigurationSection("").getKeys(false)) {

        }
        sendConsoleMessage("&6[RandomSky] &aLoaded " + loaded + " Island Item Recipes");
    }
    public void disable() {
        if(!isEnabled) return;
        config = null;
        isEnabled = false;
        ItemRecipe.deleteAll();
        HandlerList.unregisterAll(this);
    }
}
