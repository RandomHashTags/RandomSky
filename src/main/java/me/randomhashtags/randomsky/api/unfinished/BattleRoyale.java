package me.randomhashtags.randomsky.api.unfinished;

import me.randomhashtags.randomsky.RandomSkyAPI;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import java.io.File;

public class BattleRoyale extends RandomSkyAPI implements Listener, CommandExecutor {

    private static BattleRoyale instance;
    public static BattleRoyale getBattleRoyale() {
        if(instance == null) instance = new BattleRoyale();
        return instance;
    }

    public boolean isEnabled = false;
    public YamlConfiguration config;

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        final Player player = sender instanceof Player ? (Player) sender : null;
        final int l = args.length;
        return true;
    }


    public void enable() {
        final long started = System.currentTimeMillis();
        if(isEnabled) return;
        save(null, "battle royale.yml");
        config = YamlConfiguration.loadConfiguration(new File(rsd, "battle royale.yml"));
        pluginmanager.registerEvents(this, randomsky);
        isEnabled = true;
    }
    public void disable() {
        if(!isEnabled) return;
        config = null;
        isEnabled = false;
        HandlerList.unregisterAll(this);
    }
}
