package me.randomhashtags.randomsky.api.unfinished;

import me.randomhashtags.randomsky.RandomSkyAPI;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

import java.io.File;

public class CollectionChests extends RandomSkyAPI implements Listener {

    private static CollectionChests instance;
    public static final CollectionChests getCollectionChests() {
        if(instance == null) instance = new CollectionChests();
        return instance;
    }

    public boolean isEnabled = false;
    public YamlConfiguration config;

    public ItemStack collectionchest;

    public void enable() {
        final long started = System.currentTimeMillis();
        if(isEnabled) return;
        save(null, "collection chests.yml");
        pluginmanager.registerEvents(this, randomsky);
        isEnabled = true;

        config = YamlConfiguration.loadConfiguration(new File(rsd, "collection chests.yml"));
        collectionchest = d(config, "settings");
        sendConsoleMessage("&6[RandomSky] &aLoaded Collection Chests &e(took " + (System.currentTimeMillis()-started) + "ms)");
    }
    public void disable() {
        if(!isEnabled) return;
        config = null;
        collectionchest = null;
        isEnabled = false;
        HandlerList.unregisterAll(this);
    }


    @EventHandler
    private void blockPlaceEvent(BlockPlaceEvent event) {
        if(!event.isCancelled()) {
            final ItemStack i = event.getItemInHand();
            if(i.isSimilar(collectionchest)) {
                event.setCancelled(true);
                event.getPlayer().updateInventory();
            }
        }
    }
}
