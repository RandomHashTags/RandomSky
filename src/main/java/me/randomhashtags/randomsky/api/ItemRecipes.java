package me.randomhashtags.randomsky.api;

import me.randomhashtags.randomsky.RandomSkyAPI;
import me.randomhashtags.randomsky.utils.classes.island.ItemRecipe;
import org.bukkit.command.CommandExecutor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import java.io.File;
import java.util.HashMap;

public class ItemRecipes extends RandomSkyAPI implements Listener, CommandExecutor {

    private static ItemRecipes instance;
    public static final ItemRecipes getItemRecipes() {
        if(instance == null) instance = new ItemRecipes();
        return instance;
    }

    public boolean isEnabled = false;
    public YamlConfiguration config;

    public void enable() {
        final long started = System.currentTimeMillis();
        if(!isEnabled) return;
        save(null, "item recipes.yml");
        isEnabled = true;

        config = YamlConfiguration.loadConfiguration(new File(rsd, "item recipes.yml"));

        for(String s : config.getConfigurationSection("recipes").getKeys(false)) {
            new ItemRecipe(s);
        }
        final HashMap<String, ItemRecipe> r = ItemRecipe.recipes;
        sendConsoleMessage("&6[RandomSky] &aLoaded " + (r != null ? r.size() : 0) + " Item Recipes &e(took " + (System.currentTimeMillis()-started) + "ms)");
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        config = null;
        ItemRecipe.deleteAll();
        HandlerList.unregisterAll(this);
    }
}
